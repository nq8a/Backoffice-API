<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
			$table->string('code');
			$table->string('categoryId');
			$table->string('providerId');
			$table->string('providerName');
			$table->string('nameAdapter');
			$table->string('title');
			$table->string('description');
			$table->string('arrival');
			$table->string('departure');
			$table->string('qtyProduct');
            $table->string('totalPrice');
            if ((DB::connection()->getPdo()->getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') && version_compare(DB::connection()->getPdo()->getAttribute(PDO::ATTR_SERVER_VERSION), '5.7.8', 'ge')) {
                $table->json('paxList')->nullable();
                $table->json('cxlPolicy')->nullable();
                $table->json('location')->nullable();
            } else {
                $table->text('paxList')->nullable();
                $table->text('cxlPolicy')->nullable();
                $table->text('location')->nullable();
            }
			$table->string('status')->nullable();
            $table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}

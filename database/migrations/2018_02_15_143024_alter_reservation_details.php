<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReservationDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservation_details', function (Blueprint $table) {
            $table->dropColumn('first_name');
			$table->dropColumn('last_name');
			$table->dropColumn('age');
			$table->dropColumn('email');
			$table->dropColumn('phone');
			$table->dropColumn('country');
			$table->dropColumn('address');
            $table->dropColumn('holder');
            $table->dropColumn('type');
            $table->integer('roomNumber')->nullable();
            $table->string('roomType')->nullable();
            $table->string('refPrice')->nullable();
            if ((DB::connection()->getPdo()->getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') && version_compare(DB::connection()->getPdo()->getAttribute(PDO::ATTR_SERVER_VERSION), '5.7.8', 'ge')) {
                $table->json('pax');
            } else {
                $table->text('pax');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservation_details', function (Blueprint $table) {
            //
        });
    }
}

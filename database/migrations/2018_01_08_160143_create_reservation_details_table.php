<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_details', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('reservation_id')->unsigned();
			$table->string('first_name');
			$table->string('last_name');
			$table->integer('age');
			$table->string('email', 100);
			$table->string('phone');
			$table->string('country');
			$table->string('address');
			$table->string('holder', 1);
			
            $table->timestamps();
			$table->softDeletes();
			
			$table->foreign("reservation_id")->references("id")->on("reservations")->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_details');
    }
}

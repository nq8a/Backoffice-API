<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFlyItinerariesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flyItineraries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('airline', 25);
            $table->string('flyNumber', 25);
            $table->dateTime('departure');
            $table->dateTime('arrival');
            $table->string('airport', 50);
            $table->integer('itinerary')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('itinerary')->references('id')->on('itineraries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('flyItineraries');
    }
}

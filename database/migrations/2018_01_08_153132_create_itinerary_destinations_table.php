<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItineraryDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itinerary_destinations', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('itinerary_id')->unsigned();
            $table->string('name');
            $table->string('iata_code');
            $table->string('latitude');
            $table->string('longitude');
			$table->string('description');
            $table->timestamps();
			$table->softDeletes();
			
			$table->foreign("itinerary_id")->references("id")->on("itineraries")->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itinerary_destinations');
    }
}

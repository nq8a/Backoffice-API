<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationAgencieAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_agencie_agents', function (Blueprint $table) {
            $table->increments('id'); 
            $table->integer('reservation_id')->unsigned();
            $table->integer('agent_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign("reservation_id")->references("id")->on("reservations")->onDelete('cascade');
            $table->foreign("agent_id")->references("id")->on("agency_user")->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservation_agencie_agents');
    }
}

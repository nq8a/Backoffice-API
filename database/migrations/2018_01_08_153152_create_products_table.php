<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('itinerary_id')->unsigned();
			$table->integer('category_id')->unsigned();
            
            if ((DB::connection()->getPdo()->getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') && version_compare(DB::connection()->getPdo()->getAttribute(PDO::ATTR_SERVER_VERSION), '5.7.8', 'ge')) {
                $table->json('item')->nullable();
            } else {
                $table->text('item')->nullable();
            }
            $table->timestamps();
			$table->softDeletes();
			
			$table->foreign("itinerary_id")->references("id")->on("itineraries")->onDelete('cascade');
            $table->foreign("category_id")->references("id")->on("product_categorys")->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

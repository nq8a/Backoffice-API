<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItinerariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itineraries', function (Blueprint $table) {
			
            $table->increments('id');
            $table->integer('uid')->unsigned()->nullable();
			$table->integer('agent_id')->unsigned();
			$table->integer('client_id')->unsigned();
			$table->integer('status_id')->unsigned();
			$table->string('adults')->nullable();
			$table->string('childs')->nullable();
			$table->string('location')->nullable();
			$table->string('origin')->nullable();
			$table->string('destination')->nullable();
			$table->dateTime('arrivalDate')->nullable();
			$table->dateTime('departureDate')->nullable();
			$table->string('status', 20)->nullable();
			$table->boolean('active');
            $table->timestamps();
			$table->softDeletes();
			
			$table->foreign("agent_id")->references("id")->on("agency_user")->onDelete('cascade');
            $table->foreign("client_id")->references("id")->on("clients")->onDelete('cascade');
            $table->foreign("status_id")->references("id")->on("itinerary_status")->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itineraries');
    }
}

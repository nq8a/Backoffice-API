<?php

use Illuminate\Database\Seeder;

class AgencyClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agency_client')->where('id', 1)->delete();

        DB::table('agency_client')->insert([
            'id' => 1,
            'agency_id' => 1,
            'client_id' => 1
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\ReservationAgencieAgent;

class ReservationAgencieAgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data=new ReservationAgencieAgent();
        $data->id=1;
        $data->reservation_id=1;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=2;
        $data->reservation_id=2;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=3;
        $data->reservation_id=3;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=4;
        $data->reservation_id=4;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=5;
        $data->reservation_id=5;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=6;
        $data->reservation_id=6;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=7;
        $data->reservation_id=7;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=8;
        $data->reservation_id=8;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=9;
        $data->reservation_id=9;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=10;
        $data->reservation_id=10;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=11;
        $data->reservation_id=11;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=12;
        $data->reservation_id=12;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=13;
        $data->reservation_id=13;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=14;
        $data->reservation_id=14;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();

        $data=new ReservationAgencieAgent();
        $data->id=15;
        $data->reservation_id=15;
        $data->agent_id=1;
        $data->created_at=null;
        $data->updated_at=null;
        $data->deleted_at=null;
        $data->save();
    }
}

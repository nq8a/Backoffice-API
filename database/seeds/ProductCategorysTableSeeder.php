<?php

use App\ProductCategory;
use Illuminate\Database\Seeder;

class ProductCategorysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_categorys')->truncate();

        ProductCategory::insert(array(
			array(
				'name' => 'Hotels',
				'description' => 'Hoteles',
			),
			array(
				'name' => 'Tickets',
				'description' => 'Tickes',
			)
        ));
    }
}

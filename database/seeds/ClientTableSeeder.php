<?php

use App\Agency;
use App\Client;
use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->where('id', 1)->delete();

        $agency = Agency::findOrFail(1);
        $client = new Client;
        $client->id = 1;
        $client->first_name = 'First';
        $client->last_name = 'Last';
        $client->email = 'client@demo.com';
        $client->phone = '584145555555';
        $client->birth_date = '1994-10-20';
        $client->country = 'VE';
        $client->city = 'MA';
        $client->state = 'ZU';
        $client->address = 'None';
        $client->save();
        $client->agencies()->attach($agency);
    }
}

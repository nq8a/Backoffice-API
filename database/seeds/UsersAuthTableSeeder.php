<?php
use App\UserAuth;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UsersAuthTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_auth')->where('id', 1)->delete();
        DB::table('user_auth')->where('id', 2)->delete();

        $admin = new UserAuth();
        $admin->id = 1;
        $admin->name = 'Pedro Perez';
        $admin->email = 'admin@demo.com';
        $admin->enabled = true;
        $admin->role = 'admin';
        $admin->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 2;
        $sales_executive->name = 'Juan Gonzalez';
        $sales_executive->email = 'Owner1@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'Owner';
        $sales_executive->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 3;
        $sales_executive->name = 'Andres Lopez';
        $sales_executive->email = 'Owner2@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'Owner';
        $sales_executive->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 4;
        $sales_executive->name = 'Sofia Muñoz';
        $sales_executive->email = 'admin5212@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'Admin';
        $sales_executive->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 5;
        $sales_executive->name = 'Antonio Mujica';
        $sales_executive->email = 'Owner33@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'Owner';
        $sales_executive->save();

        $admin = new UserAuth();
        $admin->id = 6;
        $admin->name = 'Gianpiro Mori';
        $admin->email = 'admin55@demo.com';
        $admin->enabled = true;
        $admin->role = 'Admin';
        $admin->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 7;
        $sales_executive->name = 'Jose Roa';
        $sales_executive->email = 'user39@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'User';
        $sales_executive->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 8;
        $sales_executive->name = 'Jonath Corona';
        $sales_executive->email = 'Agentr472@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'Agent';
        $sales_executive->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 9;
        $sales_executive->name = 'Leonardo Briceño';
        $sales_executive->email = 'Agent142@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'Agent';
        $sales_executive->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 10;
        $sales_executive->name = 'Inocencio Giordi';
        $sales_executive->email = 'Agentr343@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'Agent';
        $sales_executive->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 11;
        $sales_executive->name = 'Luis Valero';
        $sales_executive->email = 'user472@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'User';
        $sales_executive->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 12;
        $sales_executive->name = 'Ellavlery Silva';
        $sales_executive->email = 'admin142@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'Agent';
        $sales_executive->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 13;
        $sales_executive->name = 'Carla Silveiro';
        $sales_executive->email = 'Owner343@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'Owner';
        $sales_executive->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 14;
        $sales_executive->name = 'Patricia Gonzalez';
        $sales_executive->email = 'Owner771@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'Owner';
        $sales_executive->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 15;
        $sales_executive->name = 'Celina Moreno';
        $sales_executive->email = 'Owner772@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'Owner';
        $sales_executive->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 16;
        $sales_executive->name = 'Ladymar Sanchez';
        $sales_executive->email = 'admin12@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'Admin';
        $sales_executive->save();

        $sales_executive = new UserAuth();
        $sales_executive->id = 17;
        $sales_executive->name = 'Deismar Bennedetti';
        $sales_executive->email = 'Owner1433@demo.com';
        $sales_executive->enabled = true;
        $sales_executive->role = 'Owner';
        $sales_executive->save();

        $admin = new UserAuth();
        $admin->id = 18;
        $admin->name = 'Jesus Rosas';
        $admin->email = 'adminj5@demo.com';
        $admin->enabled = true;
        $admin->role = 'Admin';
        $admin->save();

    }
}

<?php

use App\Agency;
use Illuminate\Database\Seeder;

class AgencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agencies')->where('id', 1)->delete();

        Agency::create([
            'id' => 1,
            'name' => 'Agencia',
            'name_contact' => 'Contact',
            'name_business' => 'Business',
			'address' => 'None',
			'email' => 'agency@demo.com',
			'phone' => '0424-5555555',
            'status' => 1,
            'created_by' => 1,
            'tax_id' => 'J-00000000'
        ]);
    }
}

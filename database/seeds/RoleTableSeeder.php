<?php

use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->where('id', 1)->delete();
        DB::table('roles')->where('id', 2)->delete();
        Role::insert(array(
			array(
				'id' => 1,
                'name' => 'Admin',
                'guard_name' => 'api',
			),
			array(
				'id' => 2,
                'name' => 'Sales Executive',
                'guard_name' => 'api',
            ),
			array(
				'id' => 3,
                'name' => 'Owner',
                'guard_name' => 'api',
            ),
            array(
				'id' => 4,
                'name' => 'Agent',
                'guard_name' => 'api',
            ),
            array(
				'id' => 5,
                'name' => 'User',
                'guard_name' => 'api',
			)
        ));
    }
}

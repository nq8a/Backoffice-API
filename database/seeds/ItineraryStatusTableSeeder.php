<?php

use App\ItineraryStatus;
use Illuminate\Database\Seeder;

class ItineraryStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('itinerary_status')->truncate();

        ItineraryStatus::insert(array(
			array(
				'name' => 'Process',
				'description' => 'En Proceso',
			),
			array(
				'name' => 'Finish',
				'description' => 'Finalizado',
			),
			array(
				'name' => 'Cancel',
				'description' => 'Cancelado',
			)
        ));
    }
}

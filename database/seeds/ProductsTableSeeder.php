<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->where('id', 1)->delete();

        Product::insert(array(
			array('id' => 1,
			'itinerary_id' => 1,
			'category_id' => 1,
            'item' => json_encode([]),
            'created_at'=>null,
            'updated_at'=>null,
            'deleted_at'=>null,
            'uid'=>1),
            array('id' => 2,
			'itinerary_id' => 1,
			'category_id' => 2,
            'item' => json_encode([]),
            'created_at'=>null,
            'updated_at'=>null,
            'deleted_at'=>null,
            'uid'=>2)

        ));
    }
}

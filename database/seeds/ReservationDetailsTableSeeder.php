<?php
use App\ReservationDetail;
use Illuminate\Database\Seeder;

class ReservationDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reservation=new ReservationDetail();
        $reservation->id=1;
        $reservation->reservation_id=1;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=120;
        $reservation->roomType='casual';
        $reservation->refPrice='1123';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=2;
        $reservation->reservation_id=2;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=1;
        $reservation->roomType='casual';
        $reservation->refPrice='122';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=3;
        $reservation->reservation_id=3;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=104;
        $reservation->roomType='casual';
        $reservation->refPrice='1369';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=4;
        $reservation->reservation_id=4;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=10;
        $reservation->roomType='casual';
        $reservation->refPrice='1458';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=5;
        $reservation->reservation_id=5;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=145;
        $reservation->roomType='casual';
        $reservation->refPrice='1123';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=6;
        $reservation->reservation_id=6;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=100;
        $reservation->roomType='casual';
        $reservation->refPrice='111';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=7;
        $reservation->reservation_id=7;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=10;
        $reservation->roomType='casual';
        $reservation->refPrice='1333';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=8;
        $reservation->reservation_id=8;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=180;
        $reservation->roomType='casual';
        $reservation->refPrice='1999';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=9;
        $reservation->reservation_id=9;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=10;
        $reservation->roomType='casual';
        $reservation->refPrice='1457';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=10;
        $reservation->reservation_id=10;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=10;
        $reservation->roomType='casual';
        $reservation->refPrice='1777';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=11;
        $reservation->reservation_id=11;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=10;
        $reservation->roomType='casual';
        $reservation->refPrice='1';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=12;
        $reservation->reservation_id=12;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=10;
        $reservation->roomType='casual';
        $reservation->refPrice='1';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=13;
        $reservation->reservation_id=13;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=10;
        $reservation->roomType='casual';
        $reservation->refPrice='1';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=14;
        $reservation->reservation_id=14;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=10;
        $reservation->roomType='casual';
        $reservation->refPrice='1';
        $reservation->pax='0';
        $reservation->save();

                $reservation=new ReservationDetail();
        $reservation->id=15;
        $reservation->reservation_id=15;
        $reservation->created_at=null;
        $reservation->updated_at=null;
        $reservation->deleted_at=null;
        $reservation->roomNumber=10;
        $reservation->roomType='casual';
        $reservation->refPrice='1';
        $reservation->pax='0';
        $reservation->save();
    }
}

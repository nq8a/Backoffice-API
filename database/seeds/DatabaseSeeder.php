<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Schema\Blueprint;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		
        $this->call(RoleTableSeeder::class);
        $this->call(ApplicationsTableSeeder::class);
        $this->call(AgencyTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AgentTableSeeder::class);
        $this->call(ClientTableSeeder::class);
        $this->call(AgencyClientTableSeeder::class);
        $this->call(ItineraryStatusTableSeeder::class);
        $this->call(ItineraryTableSeeder::class);
        $this->call(ProductCategorysTableSeeder::class);
        $this->call(UsersAuthTableSeeder::class);
		$this->call(ReservationsTableSeeder::class);
        $this->call(ReservationProductsTableSeeder::class);
        $this->call(ReservationDetailsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ReservationAgencieAgentSeeder::class);
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}

<?php

use App\Application;
use Illuminate\Database\Seeder;

class ApplicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('applications')->where('id', 1)->delete();
        DB::table('applications')->where('id', 2)->delete();
        DB::table('applications')->where('id', 3)->delete();

        $applications = new Application;
        $applications->name = 'USBlick-BackOffice';
        $applications->description = 'BackOffice';
        $applications->save();

        $api = new Application;
        $api->name = 'USBlick-API';
        $api->description = 'API';
        $api->save();

        $app = new Application;
        $app->name = 'USBlick-APP';
        $app->description = 'APP';
        $app->save();
    }
}

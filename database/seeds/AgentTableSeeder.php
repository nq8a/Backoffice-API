<?php

use App\Agent;
use Illuminate\Database\Seeder;

class AgentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agency_user')->where('id', 1)->delete();
        DB::table('agency_user')->where('id', 2)->delete();
        
        $agent = new Agent;
        $agent->agency_id = 1;
        $agent->user_id = 1;
        $agent->save();

        $agent = new Agent;
        $agent->agency_id = 1;
        $agent->user_id = 2;
        $agent->save();

        $agent = new Agent;
        $agent->agency_id = 1;
        $agent->user_id = 3;
        $agent->save();

        $agent = new Agent;
        $agent->agency_id = 1;
        $agent->user_id = 4;
        $agent->save();

        $agent = new Agent;
        $agent->agency_id = 1;
        $agent->user_id = 5;
        $agent->save();

    }
}

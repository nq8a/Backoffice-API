<?php

use App\User;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->where('id', 1)->delete();
        DB::table('users')->where('id', 2)->delete();

        $role_admin = Role::where('name', 'Admin')->first();
        $role_owner  = Role::where('name', 'Owner')->first();

        $admin = new User();
        $admin->id = 1;
        $admin->first_name = 'Pedro';
        $admin->last_name = 'Perez';
        $admin->email = 'admin@demo.com';
        $admin->enabled = true;
        $admin->save();
        $admin->assignRole($role_admin);

        $sales_executive = new User();
        $sales_executive->id = 2;
        $sales_executive->first_name = 'Juan';
        $sales_executive->last_name = 'Gonzalez';
        $sales_executive->enabled = true;
        $sales_executive->email = 'owner@demo.com';
        $sales_executive->save();
        $sales_executive->assignRole($role_owner);

        $sales_executive = new User();
        $sales_executive->id = 3;
        $sales_executive->first_name = 'Carol';
        $sales_executive->last_name = 'Mendez';
        $sales_executive->enabled = true;
        $sales_executive->email = 'admin3@demo.com';
        $sales_executive->save();
        $sales_executive->assignRole($role_admin);

        $sales_executive = new User();
        $sales_executive->id = 4;
        $sales_executive->first_name = 'Cristina';
        $sales_executive->last_name = 'Lopez';
        $sales_executive->enabled = true;
        $sales_executive->email = 'admin4@demo.com';
        $sales_executive->save();
        $sales_executive->assignRole($role_admin);

        $sales_executive = new User();
        $sales_executive->id = 5;
        $sales_executive->first_name = 'Nelson';
        $sales_executive->last_name = 'Porraz';
        $sales_executive->enabled = true;
        $sales_executive->email = 'owner5@demo.com';
        $sales_executive->save();
        $sales_executive->assignRole($role_owner);
    }
}

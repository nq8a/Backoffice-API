<?php

use App\Itinerary;
use Illuminate\Database\Seeder;

class ItineraryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('itineraries')->where('id', 1)->delete();

        Itinerary::create(array(
			'id' => 1,
			'uid' => 1,
			'agent_id' => 1,
			'client_id' => 1,
			'status_id' => 1,
			'TITLE' => 'TITLE',
			'location' => 'VE',
			'origin' => 'VE',
			'arrivalDate' => '2018-01-12',
			'departureDate' => '2018-01-15',
			'status' => 'Draft',
			'active' => 1,
        ));
    }
}

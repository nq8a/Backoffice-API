<?php

namespace App;

use App\Agency;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(definition="Agents", type="object", 
 *  	allOf={ 
 *      @SWG\Schema(
 *           required={"agency_id"},
 *           @SWG\Property(property="agency_id", format="integer", type="integer", description="Unique identifier of an agency")
 *       ),
 *      @SWG\Schema(
 *           required={"user_id"},
 *           @SWG\Property(property="user_id", format="integer", type="integer", description="Unique identifier of a user")
 *       )
 *  	}
 * )
 */
class Agent extends Model
{
    /**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'agency_user';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
	}
	
	public function agency()
    {
        return $this->hasMany('App\Agency', 'agency_id', 'id');
    }

    public function itinerarys()
    {
        return $this->hasMany('App\Itinerary');
	}

	
	
	public function delete()
    {
        // delete all related itineraries
        $this->itinerarys()->delete();
       // $this->agency()->delete();      
        // delete the agent
        return parent::delete();
    }
}

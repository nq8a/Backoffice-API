<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(definition="Applications", type="object", 
 *  	allOf={ 
 *      @SWG\Schema(
 *           required={"name"},
 *           @SWG\Property(property="name", format="string", type="string")
 *       ),
 *      @SWG\Schema(
 *           required={"description"},
 *           @SWG\Property(property="description", format="string", type="string")
 *       )
 *  	}
 * )
 */
class Application extends Model
{
    /**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'applications';
	
	public function agencies()
    {
        return $this->hasMany('App\Agency', 'created_by', 'id');
	}
	
	public function delete()
    {
        // delete all related agencies
		$this->agencies()->delete();
		
        // delete the Application
        return parent::delete();
    }
}

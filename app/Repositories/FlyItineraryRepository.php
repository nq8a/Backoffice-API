<?php

namespace App\Repositories;

use App\Models\FlyItinerary;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class FlyItineraryRepository
 * @package App\Repositories
 * @version March 23, 2018, 2:04 pm UTC
 *
 * @method FlyItinerary findWithoutFail($id, $columns = ['*'])
 * @method FlyItinerary find($id, $columns = ['*'])
 * @method FlyItinerary first($columns = ['*'])
*/
class FlyItineraryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FlyItinerary::class;
    }
}

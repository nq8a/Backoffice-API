<?php

namespace App\Repositories;

use App\Models\prueba;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class pruebaRepository
 * @package App\Repositories
 * @version March 19, 2018, 3:39 pm UTC
 *
 * @method prueba findWithoutFail($id, $columns = ['*'])
 * @method prueba find($id, $columns = ['*'])
 * @method prueba first($columns = ['*'])
*/
class pruebaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'number'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return prueba::class;
    }
}

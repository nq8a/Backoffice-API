<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class Role extends Model
{
    use Notifiable;
	use HasRoles;
	

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */

    	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    public $table = 'roles';

        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'guard_name', 'created_at', 'update_at'
    ];

    public function user_roles()
    {
        return $this->hasMany('App\Agent');
    }
    public function users()
    {
        return $this->hasMany('App\User');
    }
}

<?php

namespace App;

use App\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

/**
 * @SWG\Definition(definition="Users", type="object", 
 *  allOf={ 
 *      @SWG\Schema(
 *           required={"first_name"},
 *           @SWG\Property(property="first_name", format="string", type="string")
 *       ),
 *      @SWG\Schema(
 *           required={"last_name"},
 *           @SWG\Property(property="last_name", format="string", type="string")
 *       ),
 *      @SWG\Schema(
 *           required={"phone"},
 *           @SWG\Property(property="phone", format="phone", type="string")
 *       ),
 *      @SWG\Schema(
 *           required={"email"},
 *           @SWG\Property(property="email", format="email", type="string")
 *       ),
 *      @SWG\Schema(
 *           required={"password"},
 *           @SWG\Property(property="password", format="string", type="string")
 *       ),
 *      @SWG\Schema(
 *           required={"password_confirmation"},
 *           @SWG\Property(property="password_confirmation", format="string", type="string")
 *       ),
 *      @SWG\Schema(
 *           required={"enabled"},
 *           @SWG\Property(property="enabled", format="boolean", type="boolean")
 *       )
 *  },
 *      @SWG\Property(
 * 			property="roles", 
 * 			format="array", 
 * 			type="array",
 *          description="Array of integers with the code of a role",
 *          @SWG\Items(
 *              type="integer")
 * 		),
 *      @SWG\Property(
 * 			property="agency_id", 
 * 			format="integer", 
 * 			type="integer",
 *          description="Unique identifier of an agency"
 * 		)
 * )
 */

/**
 * @SWG\Definition(definition="Permissions", type="object", 
 *  allOf={ 
 *      @SWG\Schema(
 *           required={"name"},
 *           @SWG\Property(property="name", format="string", type="string")
 *       ),
 *  },
 *      @SWG\Property(
 * 			property="roles", 
 * 			format="array", 
 * 			type="array",
 *          description="Array of integers with the code of a role",
 *          @SWG\Items(
 *              type="integer")
 * 		)
 * )
 */

/**
 * @SWG\Definition(definition="Roles", type="object", 
 *  allOf={ 
 *      @SWG\Schema(
 *           required={"name"},
 *           @SWG\Property(property="name", format="string", type="string")
 *       ),
 *  },
 *      @SWG\Property(
 * 			property="permissions", 
 * 			format="array", 
 * 			type="array",
 *          description="Array of integers with the code of a permission",
 *          @SWG\Items(
 *              type="integer")
 * 		)
 * )
 */

/**
 * @SWG\Definition(definition="PermissionUpdate", type="object", 
 *  allOf={ 
 *      @SWG\Schema(
 *           required={"name"},
 *           @SWG\Property(property="name", format="string", type="string")
 *       ),
 *  }
 * )
 */
class User extends Authenticatable
{
    use Notifiable;
	use HasRoles;
	
	/**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'enabled',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'remember_token'
    ];

    public function agent()
    {
        return $this->hasMany('App\Agent');
    }
}

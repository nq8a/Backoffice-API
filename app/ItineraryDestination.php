<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(definition="ItineraryDestinations", type="object", 
 *  allOf={ 
 *      @SWG\Schema(
 *           required={"itinerary_id"},
 *           @SWG\Property(property="itinerary_id", format="integer", type="integer", description="Unique identifier of itinerary")
 *      ),
 * 		@SWG\Schema(
 *           required={"iata_code"},
 *           @SWG\Property(property="iata_code", format="string", type="string")
 *      )
 *  }
 * )
 */
class ItineraryDestination extends Model
{
    /**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'itinerary_destinations';
	
	public function agent()
    {
        return $this->belongsTo('App\Agent');
    }
	
	public function clients()
    {
        return $this->belongsTo('App\Client', 'client_id', 'id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(definition="ReservationDetails", type="object", 
 *  allOf={ 
 *      @SWG\Schema(
 *           required={"reservation_id"},
 *           @SWG\Property(property="reservation_id", format="integer", type="integer", description="Unique identifier of a reservation")
 *       ),
 *      @SWG\Schema(
 *           required={"roomNumber"},
 *           @SWG\Property(property="roomNumber", format="integer", type="integer")
 *       ),
 * 		@SWG\Schema(
 *           required={"roomType"},
 *           @SWG\Property(property="roomType", format="string", type="string")
 *       ),
 * 		@SWG\Schema(
 *           required={"refPrice"},
 *           @SWG\Property(property="refPrice", format="string", type="string")
 *       ),
 * 		@SWG\Schema(
 *           required={"pax"},
 *           @SWG\Property(property="pax", format="array", type="array", description="Pax details information",
 * 				@SWG\Items(type="object")
 * 			)
 *       )
 *  }
 * )
 */
class ReservationDetail extends Model
{
    /**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'reservation_details';
}

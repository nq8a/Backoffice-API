<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(definition="Itineraries", type="object", 
 *  	allOf={
 *          @SWG\Schema(
 *              required={"agent_id"},
 *              @SWG\Property(property="agent_id", format="integer", type="integer", description="Unique identifier of agent")
 *          ), 
 *          @SWG\Schema(
 *              required={"status_id"},
 *              @SWG\Property(property="status_id", format="integer", type="integer", description="Reference to a unique identifier in ItineraryStatus")
 *          ), 
 *          @SWG\Schema(
 *              required={"active"},
 *              @SWG\Property(property="active", format="boolean", type="boolean")
 *          ),
 *          @SWG\Schema(
 *              required={"title"},
 *              @SWG\Property(property="title", format="string", type="string")
 *          )
 *  	},
 *      @SWG\Property(
 * 			property="uid", 
 * 			format="string", 
 * 			type="string",
 *          description="Unique identifier provided by USBlick app"
 * 		),
 *      @SWG\Property(
 * 			property="items", 
 * 			format="array", 
 * 			type="array",
 *          description="Json array with UID of product provided by USBlick-app and details",
 *          @SWG\Items(
 *              @SWG\Property(property="id", format="string", type="string", description="This is UID of a product")
 *          )
 * 		),
 *      @SWG\Property(
 * 			property="destination", 
 * 			format="array", 
 * 			type="array",
 *          description="Json array with iata_code of a city provided by octopus",
 *          @SWG\Items(
 *              @SWG\Property(property="iata_code", format="string", type="string", description="IATA Code of the city")
 *          )
 * 		),
 * 		@SWG\Property(
 * 			property="client_id", 
 * 			format="integer", 
 * 			type="integer",
 *          description="Unique identifier of a client"
 * 		),
 *      @SWG\Property(
 * 			property="status", 
 * 			format="string", 
 * 			type="string"
 * 		),
 *      @SWG\Property(
 * 			property="adults", 
 * 			format="string", 
 * 			type="string"
 * 		),
 *      @SWG\Property(
 * 			property="childs", 
 * 			format="string", 
 * 			type="string"
 * 		),
 *      @SWG\Property(
 * 			property="origin", 
 * 			format="string", 
 * 			type="string"
 * 		),
 * 		@SWG\Property(
 * 			property="location", 
 * 			format="string", 
 * 			type="string"
 * 		),
 *      @SWG\Property(
 * 			property="arrivalDate", 
 * 			format="date-time", 
 * 			type="string"
 * 		),
 *      @SWG\Property(
 * 			property="departureDate", 
 * 			format="date-time", 
 * 			type="string"
 * 		)
 * )
 */

class Itinerary extends Model
{
    /**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'itineraries';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'uid',
		'agent_id',
        'client_id',
        'status_id',
        'adults',
        'childs',
        'title',
        'location',
        'origin',
        'destination',
        'arrivalDate',
        'departureDate',
        'status',
        'active',
    ];
	
	public function agent()
    {
        return $this->belongsTo('App\Agent');
    }
	
	public function clients()
    {
        return $this->belongsTo('App\Client', 'client_id', 'id');
    }
	
	public function state()
    {
        return $this->belongsTo('App\ItineraryStatus', 'status_id', 'id');
    }
	
	public function destinations()
    {
        return $this->hasMany('App\ItineraryDestination');
    }
	
	public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function fly()
    {
        return $this->hasOne(\App\Models\FlyItinerary::class,'itinerary');
    }

    public function delete()
    {
        // delete all related destinations
        $this->destinations()->delete();
        
        // delete all related products
        $this->products()->delete();

        // delete the itinerary
        return parent::delete();
    }
}

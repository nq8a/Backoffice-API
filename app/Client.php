<?php

namespace App;

use App\Agency;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

/**
 * @SWG\Definition(definition="Clients", type="object", 
 *  	allOf={ 
 *      @SWG\Schema(
 *           required={"first_name"},
 *           @SWG\Property(property="first_name", format="string", type="string")
 *       ),
 *      @SWG\Schema(
 *           required={"phone"},
 *           @SWG\Property(property="phone", format="phone", type="string")
 *       ),
 *      @SWG\Schema(
 *           required={"email"},
 *           @SWG\Property(property="email", format="email", type="string")
 *       ),
 *  	},
 * 		@SWG\Property(
 * 			property="last_name", 
 * 			format="string", 
 * 			type="string"
 * 		),
 * 		@SWG\Property(
 * 			property="country", 
 * 			format="string", 
 * 			type="string"
 * 		),
 * 		@SWG\Property(
 * 			property="city", 
 * 			format="string", 
 * 			type="string"
 * 		),
 * 		@SWG\Property(
 * 			property="state", 
 * 			format="string", 
 * 			type="string"
 * 		),
 * 		@SWG\Property(
 * 			property="address", 
 * 			format="string", 
 * 			type="string"
 * 		),
 * 		@SWG\Property(
 * 			property="birth_date", 
 * 			format="date", 
 * 			type="string"
 * 		)
 * )
 */

 /**
 * @SWG\Definition(definition="AgencyClient", type="object", 
 *  	allOf={ 
 *      @SWG\Schema(
 *           required={"agency_id"},
 *           @SWG\Property(property="agency_id", format="integer", type="integer")
 *       ),
 *      @SWG\Schema(
 *           required={"client_id"},
 *           @SWG\Property(property="client_id", format="integer", type="integer")
 *       )
 *  	}
 * )
 */
class Client extends Model
{
    /**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'clients';
	
	public function itineraries()
    {
        return $this->hasMany('App\Itinerary');
	}
	
	public function agencies()
	{
		return $this->belongsToMany('App\Agency');
	}

    public function scopeName($query, $name)
    {
        if(trim($name) != ''){
            $query->where(\DB::raw("CONCAT(first_name, ' ', last_name)"), "LIKE", "%$name%");
        }
	}
	
	public function delete()
    {
        // delete all related itineraries
        $this->itineraries()->delete();

        // delete the client
        return parent::delete();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(definition="Products", type="object", 
 *  	allOf={
 * 			@SWG\Schema(
 *           	required={"itinerary_id"},
 *           	@SWG\Property(property="itinerary_id", format="integer", type="integer", description="Unique identifier of itinerary")
 *       	), 
 * 			@SWG\Schema(
 *           	required={"category_id"},
 *           	@SWG\Property(property="category_id", format="integer", type="integer", description="Unique identifier of category")
 *       	), 
 *      	@SWG\Schema(
 *           	required={"uid"},
 *           	@SWG\Property(property="uid", format="string", type="string")
 *       	),
 *  	},
 * 		@SWG\Property(
 * 			property="item", 
 * 			format="object", 
 * 			type="object"
 * 		),
 * )
 */

 /**
 * @SWG\Definition(definition="ProductsUpdate", type="object", 
 *  	allOf={
 * 			@SWG\Schema(
 *           	required={"item"},
 *           	@SWG\Property(
 * 			        property="item", 
 * 			        format="object", 
 * 			        type="object",
 *                  description="Json array with itinerary_id provided by app and details",
 *                  @SWG\Property(property="itinerary_id", format="integer", type="integer")
 * 		        )
 *       	)
 *  	}
 * )
 */


class Product extends Model
{
    /**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'products';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'uid',
		'itinerary_id',
        'category_id',
        'item',
    ];
	
	public function category()
    {
        return $this->belongsTo('App\ProductCategory');
	}

	public function reservation()
    {
        return $this->belongsToMany('App\Reservation');
	}
	
	public function delete()
    {
        $this->reservation()->detach();

        // delete the product
        return parent::delete();
    }
}

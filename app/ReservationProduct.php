<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(definition="ReservationProducts", type="object", 
 *  allOf={ 
 *      @SWG\Schema(
 *           required={"reservation_id"},
 *           @SWG\Property(property="reservation_id", format="integer", type="integer", description="Unique identifier of a reservation")
 *       ),
 *      @SWG\Schema(
 *           required={"product_id"},
 *           @SWG\Property(property="product_id", format="integer", type="integer", description="Unique identifier of a product")
 *       )
 *  }
 * )
 */
class ReservationProduct extends Model
{
    /**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'product_reservation';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
		'reservation_id',
		'product_id',
    ];
}

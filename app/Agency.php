<?php

namespace App;

use App\Client;
use App\Agent;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * @SWG\Definition(definition="Agencies", type="object", 
 *  	allOf={ 
 *      @SWG\Schema(
 *           required={"name"},
 *           @SWG\Property(property="name", format="string", type="string")
 *       ),
 *      @SWG\Schema(
 *           required={"name_contact"},
 *           @SWG\Property(property="name_contact", format="string", type="string")
 *       ),
 *      @SWG\Schema(
 *           required={"name_business"},
 *           @SWG\Property(property="name_business", format="string", type="string")
 *       ),
 * 		@SWG\Schema(
 *           required={"address"},
 *           @SWG\Property(property="address", format="string", type="string")
 *       ),
 * 		@SWG\Schema(
 *           required={"email"},
 *           @SWG\Property(property="email", format="string", type="string")
 *       ),
 * 		@SWG\Schema(
 *           required={"phone"},
 *           @SWG\Property(property="phone", format="string", type="string")
 *       ),
 * 		@SWG\Schema(
 *           required={"status"},
 *           @SWG\Property(property="status", format="boolean", type="boolean")
 *       ),
 * 		@SWG\Schema(
 *           required={"created_by"},
 *           @SWG\Property(property="created_by", format="integer", type="integer")
 *       ),
 * 		@SWG\Schema(
 *           required={"tax_id"},
 *           @SWG\Property(property="tax_id", format="string", type="string")
 *       )
 *  	}
 * )
 */
class Agency extends Model
{
    /**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'agencies';
	
	public function users()
    {
        return $this->belongsToMany('App\User');
	}

	public function application()
    {
		return $this->belongsTo('App\Application', 'created_by', 'id');
	}
	
	public function clients()
	{
		return $this->belongsToMany('App\Client');
	}

	public function agent()
	{
		return $this->hasMany('App\Agent');
	}

	public function delete()
    {
        $this->clients()->detach();
        $this->users()->detach();
		//$this->agent()->detach();
		
        // delete the itinerary
        return parent::delete();
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReservationAgencieAgent extends Model
{
        
    /**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;


	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
        protected $dates = [
		'deleted_at'
	];

		/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'reservation_agencie_agents';
}

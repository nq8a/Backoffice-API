<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\ReservationsAgenciesController;
use App\Http\Controllers\API\ReservationAgencieAgent;
use Illuminate\Support\Facades\DB;

class ReservationsAgenciesController extends Controller
{
	public function ListForAgencie($id){

		$res = DB::table('reservation_agencie_agents')
		->select('reservation_agencie_agents.reservation_id','reservations.categoryId','reservations.providerId','reservations.providerName','reservations.nameAdapter','reservations.title','reservations.description','reservations.arrival','reservations.departure','reservations.qtyProduct','reservations.totalPrice','reservations.paxList','reservations.cxlPolicy','reservations.location','reservations.status','reservations.created_at','reservations.updated_at','reservations.deleted_at','reservations.uid','reservations.commission','reservations.nights','reservations.payBefore','reservations.company','agency_user.agency_id','agencies.name','agencies.name_contact','agencies.name_business','agencies.address','agencies.email','agencies.phone','agencies.status','agencies.created_at','agencies.updated_at','agencies.deleted_at','agencies.created_by','agencies.tax_id')
		->join('reservations','reservations.id','=','reservation_agencie_agents.reservation_id')
		->join('agency_user','agency_user.id','=','reservation_agencie_agents.agent_id')
		->join('agencies','agencies.id','=','agency_user.agency_id')
		->where('agency_user.agency_id',$id)
		->get();

		$agent = DB::table('agency_user')
		->select('users.id','users.first_name','users.last_name','users.created_at','users.updated_at','users.deleted_at','users.enabled')
		->join('users','users.id','=','agency_user.user_id')
		->where('agency_user.agency_id',$id)
		->get();

		$product = DB::table('product_reservation')
		->select('product_categorys.name','product_categorys.id','reservation_agencie_agents.reservation_id')
		->join('products','products.id','=','product_reservation.product_id')
		->join('product_categorys','product_categorys.id','=','products.category_id')
		->join('reservation_agencie_agents','reservation_agencie_agents.reservation_id','=','product_reservation.reservation_id')
		->join('agency_user','agency_user.id','=','reservation_agencie_agents.agent_id')
		->join('agencies','agencies.id','=','agency_user.agency_id')
		->where('agency_user.agency_id',$id)
		->get();

$reservation['reservation']=$res;
$reservation['agents']=$agent;
$reservation['products']=$product;

		if($res->isEmpty()){
        return (['success' => false, 'message' => 'This agency does not have reservations']);
        }

        if($res){
        	return ['data' => $reservation];}
        
}
}

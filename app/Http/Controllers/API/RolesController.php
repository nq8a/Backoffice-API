<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\API\RequestRole;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\AppBaseController;

class RolesController extends AppBaseController
{
	/*
	 * API Response JSON
	 *
	 */
	use ApiResponse;
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/roles",
    * 	  tags={"Roles"},
    *     operationId="Roles",
    *     summary="Return list with all the roles",
    *     description="Roles list",
    *     produces={"application/json"},
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index()
    {
		// Roles Paginate: page=1
		$data = Role::latest()->paginate(10);
		
		// Response
    	return $this->respond($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Post(
    *     path="/roles",
    * 	  tags={"Roles"},
    *     operationId="addRole",
    *     summary="Create a new role and add permissions if needed",
    *     description="Create a new role and add permissions if needed",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="Role",
    *         in="body",
    *         description="JSON format to create a new role",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Roles")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestRole $request)
    {
		// Input Data
		$input = $request->all();
		
        $role = new Role();
        $role->name = $input['name'];
        $role->guard_name = 'api';
        $role->save();
    
		// Permissions
		$permissions = (isset($request['permissions']) && !empty($request['permissions'])) ? $request['permissions'] : [];
		foreach ($permissions as $id) 
		{
            $permission = Permission::where('id', '=', $id)->firstOrFail();
            $role->givePermissionTo($permission);
        }
		
		// Response
		return $this->respond(['success' => true, 'message' => 'Created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $permission
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/roles/{id}",
    * 	  tags={"Roles"},
    *     operationId="showRole",
    *     summary="Show detail of a role",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of role",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
		// Find
		if ( ! $role = Role::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Response
        return $this->respond(['data' => $role]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $permission
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Put(
    *     path="/roles/{id}",
    * 	  tags={"Roles"},
    *     operationId="updateRole",
    *     summary="Update a role",
    *     description="Update a role",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="Code of role",
    * 	  ),
    *     @SWG\Parameter(
    *         name="role",
    *         in="body",
    *         description="Json format to update a role",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Roles")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(Request $request, $id)
    {
		// Find
		if ( ! $role = Role::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Input Data
		$input = $request->all();
		
		// Update
		$role->name = $input['name'];
		$role->save();
		
		// Permissions
		$permissions = (isset($request['permissions']) && !empty($request['permissions'])) ? $request['permissions'] : [];
		
		// Permissions Remove
		$permissionAll = Permission::all();
        foreach ($permissionAll as $p) {
            $role->revokePermissionTo($p);
        }

		// Permissions Add
        foreach ($permissions as $id) {
            $permission = Permission::where('id', '=', $id)->firstOrFail();
            $role->givePermissionTo($permission);
        }
		
		// Response
		return $this->respond(['success' => true, 'message' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $permission
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Delete(
    *     path="/roles/{id}",
    * 	  tags={"Roles"},
    *     operationId="DeleteRole",
    *     summary="Delete one role",
    *     description="Delete one role",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of role",
    * 		),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy($id)
    {
		// Find
		if ( ! $role = Role::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Destroy
        $role->delete();
		
		// Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

<?php

namespace App\Http\Controllers\API;

use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Requests\API\RequestProductCategory;
use App\Http\Controllers\AppBaseController;

class ProductCategorysController extends AppBaseController
{
	/*
	 * API Response JSON
	 *
	 */
	use ApiResponse;
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/products/category",
    * 	  tags={"ProductCategory"},
    *     operationId="Categories",
    *     summary="Return list with all categories",
    *     description="Categories list",
    *     produces={"application/json"},
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index()
    {
        // Category Paginate: page=1
        $data = ProductCategory::latest()->paginate(10);
		
        // Response
        return $this->respond($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Post(
    *     path="/products/categories",
    * 	  tags={"ProductCategory"},
    *     operationId="addCategory",
    *     summary="Create a new category",
    *     description="Create a new category",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="Category",
    *         in="body",
    *         description="JSON format to create a new category",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/ProductCategories")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestProductCategory $request)
    {
		// Input Data
        $input = $request->all();

        // News
        $product = new ProductCategory();
        $product->name 			= $input['name'];
        $product->description 	= $input['description'];
        $product->save();

        // Response
        return $this->respond(['success' => true, 'message' => 'Created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCategory  $product
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/products/category/{id}",
    * 	  tags={"ProductCategory"},
    *     operationId="showCategory",
    *     summary="Show detail of a category",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of category",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
        // Find
		if ( ! $product = ProductCategory::with(['agent', 'clients'])->find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Response
        return $this->respond(['data' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCategory  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCategory  $product
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Put(
    *     path="/products/category/{id}",
    * 	  tags={"ProductCategory"},
    *     operationId="updateCategory",
    *     summary="Update a category",
    *     description="Update a category",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="Code of category",
    * 	  ),
    *     @SWG\Parameter(
    *         name="category",
    *         in="body",
    *         description="Json format to update a category",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/ProductCategories")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(RequestProductCategory $request, $id)
    {
        // Find
		if ( ! $product = ProductCategory::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}

        // Input Data
        $input = $request->all();

        // Update
        $product->name 			= $input['name'];
        $product->description 	= $input['description'];
        $product->save();

        // Response
        return $this->respond(['success' => true, 'message' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCategory  $product
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Delete(
    *     path="/products/category/{id}",
    * 	  tags={"ProductCategory"},
    *     operationId="DeleteCategory",
    *     summary="Delete one category",
    *     description="Delete one category",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of category",
    * 		),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy($id)
    {
        // Find
		if ( ! $product = ProductCategory::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Destroy
        $product->delete();

        // Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

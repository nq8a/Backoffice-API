<?php

namespace App\Http\Controllers\API;

use App\UserAuth;
use App\User;
use App\Http\Controllers\Controller;

class UserStateController extends Controller
{
    public function ListAllUser()
    {

      try {
        $emails_backoffice = User::select()->pluck('email')->toArray();
        
        $users=UserAuth::select()
        ->whereNotIn('email', $emails_backoffice)
        ->get(['id','name','email','role','enabled','created_at']);

        return response()->json(['res' =>1,'users_auth' =>$users,
                                ], 200);
      } catch (Exception $e) {
          $detalle = 'An error has ocurred';
          return response()->json(['res' =>1,'msg' =>$detalle,
                                  ], 200);
      }
    }
}

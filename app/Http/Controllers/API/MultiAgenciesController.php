<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Agent;
use App\Agency;
use App\User;
use App\Http\Requests\API\RequestAgent;
use App\Http\Controllers\AppBaseController;

class MultiAgenciesController extends AppBaseController
{
    use ApiResponse;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
                 // Input Data
                 $input = $request->all();
                 // Find User
                 if ( ! $user = User::find($input['user_id'])) {
                     $id = $input['user_id'];
                     return $this->respondNotFound("The user with id $id does not exist");
                 }
                $mensaje_fallo = "The agent already exists for the agencies with the following ids: ";
                $add=false;
                $fail=false;
                foreach ($input['agency_id'] as $agency_id)
                {
                    // Find Agent
                    if ( ! $agency = Agency::find($agency_id)){
                        $id = $agency_id;
                        return $this->respondNotFound("The agency with id $id does not exist");
                    }
                    $validar_agent = Agent::where('user_id', $input['user_id'])->where('agency_id',$agency_id)->count();
                    if($validar_agent == 0){
                        $add=true;
                        // New
                        $agent = new Agent;
                        $agent->user_id = $input['user_id'];
                        $agent->agency_id = $agency_id;
                        $agent->save();
                    }else{
                        $fail=true;
                        $mensaje_fallo .= $agency_id . ', ';
                    }
                }
                if($add==true && $fail==false){
                    return $this->respond(['success'=>true, 'message'=>'Agent added succesfully into all agencies']);
                }elseif($add==true && $fail==true){
                    return $this->respond(['success'=>true, 'message'=>$mensaje_fallo]);
                }else{
                    return $this->respond(['success'=>false, 'message'=>'Agent already exist for all agencies']);
                }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        		// Find
		if ( ! $user = User::find($id)->all()) {
			return $this->respondNotFound("The register with id $id does not exist");
        }
        $user = User::with('roles', 'agent')->find($id);
        // Response
$agent=$user->agent;
$i=0;
$agent1=json_decode($agent);
            foreach ($agent1 as $key => $value) {
                foreach ($value as $key => $val) {
                    if($key=='agency_id'){
                        $agency_id[]=$val;
                    }
                }
            }
$agencies = Agency::find($agency_id);
   return $this->respond(['data'=> $user,'data1' => $agencies]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

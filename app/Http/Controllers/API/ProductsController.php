<?php

namespace App\Http\Controllers\API;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests\API\RequestProduct;
use App\Http\Controllers\AppBaseController;

class ProductsController extends AppBaseController
{
	/*
	 * API Response JSON
	 *
	 */
	use ApiResponse;
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/products",
    * 	  tags={"Products"},
    *     operationId="Products",
    *     summary="Return list with all products",
    *     description="Products list",
    *     produces={"application/json"},
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index()
    {
        // Product Paginate: page=1
        $data = Product::with(['category', 'reservation'])->latest()->paginate(10);
		
        // Response
        return $this->respond($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Post(
    *     path="/products",
    * 	  tags={"Products"},
    *     operationId="addProduct",
    *     summary="Create a new product",
    *     description="Create a new product",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="Product",
    *         in="body",
    *         description="JSON format to create a new product",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Products")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestProduct $request)
    {
		// Input Data
        $input = $request->all();

        // News
        $product = new Product();
        $product->uid 			= $input['item']['id'];
        $product->itinerary_id 	= $input['itinerary_id'];
        $product->category_id 	= $input['category_id'];
        $product->item 			= json_encode($input['item']);
        $product->save();

        // Response
        return $this->respond(['success' => true, 'message' => 'Created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/products/{id}",
    * 	  tags={"Products"},
    *     operationId="showProduct",
    *     summary="Show detail of a product",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of product",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
        // Find
		if ( ! $product = Product::with(['category', 'reservation'])->find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Response
        return $this->respond(['data' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
	public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Put(
    *     path="/products/{id}",
    * 	  tags={"Products"},
    *     operationId="updateProduct",
    *     summary="Update a product",
    *     description="Update a product",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="UID of product",
    * 	  ),
    *     @SWG\Parameter(
    *         name="product",
    *         in="body",
    *         description="Json format to update a product",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/ProductsUpdate")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(RequestProduct $request, $id)
    {
		// Input Data
        $input = $request->all();
		
		$itinerary_id = (isset($input['item']['itinerary_id']) && !empty($input['item']['itinerary_id'])) ? $input['item']['itinerary_id'] : null;
		
        // Find
		if ( ! $product = Product::where(['uid' => $id, 'itinerary_id' => $itinerary_id])->first()) {
			return $this->respondNotFound("The register with id $id does not exist");
		}

        // Update
        $product->item = json_encode($input['item']);
        $product->save();

        // Response
        return $this->respond(['success' => true, 'message' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Delete(
    *     path="/products/{id}",
    * 	  tags={"Products"},
    *     operationId="DeleteProduct",
    *     summary="Delete one product",
    *     description="Delete one product",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="UID of product",
    * 		),
    *        @SWG\Parameter(
    * 			name="itinerary_id",
    * 			in="query", 
    *           required=true,
    *           description="Id of the itinerary associated with the product",
    * 			type="integer",
    * 	   		@SWG\Schema(
    *	 	      	required={"itinerary_id"},
    *	            @SWG\Property(property="itinerary_id", format="integer", type="integer")
    *           )
    *        ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy(RequestProduct $request, $id)
    {
        // Input Data
        $input = $request->all();

        // Find
		if ( ! $product = Product::where(['uid' => $id, 'itinerary_id' => $input['itinerary_id']])->first()) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Destroy
        $product->delete();

        // Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

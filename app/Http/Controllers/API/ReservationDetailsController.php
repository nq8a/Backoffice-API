<?php

namespace App\Http\Controllers\API;

use App\ReservationDetail;
use App\Reservation;
use Illuminate\Http\Request;
use App\Http\Requests\API\RequestReservationDetail;
use App\Http\Controllers\AppBaseController;

class ReservationDetailsController extends AppBaseController
{
	/*
	 * API Response JSON
	 *
	 */
	use ApiResponse;
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/reservations/details",
    * 	  tags={"ReservationDetails"},
    *     operationId="Details",
    *     summary="Return list with all details of the reservations",
    *     description="Details list",
    *     produces={"application/json"},
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index()
    {
        // ReservationDetail Paginate: page=1
		$data = ReservationDetail::latest()->paginate(10);
		
		// Response
    	return $this->respond($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

	public function getInput($input, $key, $type = null) 
	{
		return (isset($input[$key]) && !empty($input[$key])) ? $input[$key] : $type;
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     /** @SWG\Post(
    *     path="/reservations/details",
    * 	  tags={"ReservationDetails"},
    *     operationId="addDetails",
    *     summary="Create a new detail",
    *     description="Create a new detail",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="Detail",
    *         in="body",
    *         description="JSON format to create a new detail",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/ReservationDetails")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestReservationDetail $request)
    {
        // Input Data
        $input = $request->all();
        
        // Find
		if ( ! $reservation = Reservation::find($input['reservation_id'])) {
            $id = $input['reservation_id'];
			return $this->respondNotFound("The reservation with id $id does not exist");
        }

        // News
        $reservation = new ReservationDetail;
        $reservation->reservation_id 	= $input['reservation_id'];
		$reservation->roomNumber 		= $this->getInput($input, 'roomNumber');
		$reservation->roomType 		    = $this->getInput($input, 'roomType');
		$reservation->refPrice 		    = $this->getInput($input, 'refPrice');
		$reservation->pax 			    = json_encode($this->getInput($input, 'pax', []));
        $reservation->save();

        // Response
		return $this->respond(['success' => true, 'message' => 'Created successfully']);     
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReservationDetail  $reservation
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/reservations/details/{id}",
    * 	  tags={"ReservationDetails"},
    *     operationId="showDetail",
    *     summary="Show one detail",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of detail",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
		// Find
		if ( ! $reservation = ReservationDetail::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
        }
        
        $reservation->pax = json_decode($reservation->pax, true);
		
        // Response
        return $this->respond(['data' => $reservation]);
    }

    /**
     * Display the specified resource by reservation_id.
     *
     * @param  \App\ReservationDetail  $reservation
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/reservations/details/reservation_id/{id}",
    * 	  tags={"ReservationDetails"},
    *     operationId="showDetailByReservation",
    *     summary="Show details of a reservation",
    *     description="Show details of a reservation",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of reservation",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function showByReservation($id)
    {
		// Find
		if ( ! $reservation = ReservationDetail::where('reservation_id', $id)->first()) {
			return $this->respondNotFound("The register with reservation_id $id does not exist");
		}
		
        // Response
        return $this->respond(['data' => $reservation]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReservationDetail  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReservationDetail  $reservation
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Put(
    *     path="/reservations/details/{id}",
    * 	  tags={"ReservationDetails"},
    *     operationId="updateDetail",
    *     summary="Update a detail",
    *     description="Update a detail",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="Code of detail",
    * 	  ),
    *     @SWG\Parameter(
    *         name="detail",
    *         in="body",
    *         description="Json format to update a detail",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/ReservationDetails")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(RequestReservationDetail $request, $id)
    {
        // Find
		if ( ! $reservation = ReservationDetail::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Input Data
        $input = $request->all();
        
        // Update
        $reservation->reservation_id 	= $input['reservation_id'];
		$reservation->roomNumber 		= $this->getInput($input, 'roomNumber');
		$reservation->roomType 		    = $this->getInput($input, 'roomType');
		$reservation->refPrice 		    = $this->getInput($input, 'refPrice');
		$reservation->pax 			    = json_encode($this->getInput($input, 'pax', []));
        $reservation->save();

        // Response
		return $this->respond(['success' => true, 'message' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReservationDetail  $reservation
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Delete(
    *     path="/reservations/details/{id}",
    * 	  tags={"ReservationDetails"},
    *     operationId="DeleteDetail",
    *     summary="Delete one detail",
    *     description="Delete one detail",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of detail",
    * 		),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy($id)
    {
		// Find
		if ( ! $reservation = ReservationDetail::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
        // Destroy
        $reservation->delete();
		
		// Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

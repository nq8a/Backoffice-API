<?php

namespace App\Http\Controllers\API;

use App\Application;
use Illuminate\Http\Request;
use App\Http\Requests\API\RequestApplication;
use App\Http\Controllers\AppBaseController;

class ApplicationsController extends AppBaseController
{
    /*
	 * API Response JSON
	 *
	 */
    use ApiResponse;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/applications",
    * 	  tags={"Applications"},
    *     operationId="applications",
    *     summary="Return list with all applications available",
    *     description="Applications list",
    *     produces={"application/json"},
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index()
    {
        // Applications Paginate: page=1
        $data = Application::with(['agencies'])->latest()->paginate(10);

        // Response
        return $this->respond($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Post(
    *     path="/applications",
    * 	  tags={"Applications"},
    *     operationId="addApplication",
    *     summary="Create a new application",
    *     description="Create a new application",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="Application",
    *         in="body",
    *         description="JSON format to create a new application",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Applications")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestApplication $request)
    {
        // Input Data
		$input = $request->all();
		
		// News
		$application               = new Application;
		$application->name 		   = $input['name'];
		$application->description  = $input['description'];
		$application->save();
		
		// Response
		return $this->respond(['success' => true, 'message' => 'Created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/applications/{id}",
    * 	  tags={"Applications"},
    *     operationId="showApplication",
    *     summary="Show detail of an application",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of the application",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
        // Find
		if ( ! $application = Application::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
        }
        
		$application = Application::with('agencies')->find($id);
        // Response
        return $this->respond(['data' => $application]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Put(
    *     path="/applications/{id}",
    * 	  tags={"Applications"},
    *     operationId="updateApplication",
    *     summary="Update an application",
    *     description="Update an application",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="Code of application",
    * 	  ),
    *     @SWG\Parameter(
    *         name="application",
    *         in="body",
    *         description="Json format to update an application",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Applications")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(RequestApplication $request, $id)
    {
        // Find
		if ( ! $application = Application::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Input Data
		$input = $request->all();
		
		// Update
		$application->name 		    = $input['name'];
		$application->description   = $input['description'];
		$application->save();
		
		// Response
		return $this->respond(['success' => true, 'message' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Delete(
    *     path="/applications/{id}",
    * 	  tags={"Applications"},
    *     operationId="DeleteApplication",
    *     summary="Delete one application",
    *     description="Delete one application",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of application",
    * 		),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy($id)
    {
        // Find
		if ( ! $application = Application::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Destroy
        $application->delete();
		
		// Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

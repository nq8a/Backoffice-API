<?php

namespace App\Http\Controllers\API;

use App\Agent;
use App\Agency;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\API\RequestAgent;
use App\Http\Controllers\AppBaseController;

class AgentsController extends AppBaseController
{
    /*
	 * API Response JSON
	 *
	 */
    use ApiResponse;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/agents",
    * 	  tags={"Agents"},
    *     operationId="agents",
    *     summary="Return list with all agents available",
    *     description="Agents list",
    *     produces={"application/json"},
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index()
    {
        // Agents Paginate: page=1
        $data = Agent::with('user')->latest()->paginate(10);

        // Response
        return $this->respond($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */

    /** @SWG\Post(
    *     path="/agents",
    * 	  tags={"Agents"},
    *     operationId="addAgent",
    *     summary="Create a new agent",
    *     description="Create a new agent",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="Agent",
    *         in="body",
    *         description="JSON format to create a new agent",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Agents")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestAgent $request)
    {
         // Input Data
         $input = $request->all();
         
         // Find User
         if ( ! $user = User::find($input['user_id'])) {
             $id = $input['user_id'];
             return $this->respondNotFound("The user with id $id does not exist");
         }

         // Find Agent
         if ( ! $agency = Agency::find($input['agency_id'])) {
            $id = $input['agency_id'];
            return $this->respondNotFound("The agency with id $id does not exist");
        }

         // New
         $agent = new Agent;
         $agent->user_id = $input['user_id'];
         $agent->agency_id = $input['agency_id'];
         $agent->save();
        
         // Response
         return $this->respond(['success' => true, 'message' => 'Created successfully']);
 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/agents/{id}",
    * 	  tags={"Agents"},
    *     operationId="showAgent",
    *     summary="Show detail of an agent",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of the agent",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
        // Find
		if ( ! $agent = Agent::with('user')->find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
        // Response
        return $this->respond(['data' => $agent]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Put(
    *     path="/agents/{id}",
    * 	  tags={"Agents"},
    *     operationId="updateAgent",
    *     summary="Update an agent",
    *     description="Update an agent",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="Code of agent",
    * 	  ),
    *     @SWG\Parameter(
    *         name="agent",
    *         in="body",
    *         description="Json format to update an agent",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Agents")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(RequestAgent $request, $id)
    {
        // Find
		if ( ! $agent = Agent::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Input Data
        $input = $request->all();

        $agent->user_id     = $input['user_id'];
        $agent->agency_id   = $input['agency_id'];
        $agent->save();

        // Response
		return $this->respond(['success' => true, 'message' => 'Updated successfully']);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Delete(
    *     path="/agents/{id}",
    * 	  tags={"Agents"},
    *     operationId="DeleteAgent",
    *     summary="Delete one agent",
    *     description="Delete one agent",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of agent",
    * 		),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy($id)
    {
        // Find
		if ( ! $agent = Agent::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
        // Destroy
        $agent->delete();
		
		// Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\API\RequestPermission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\AppBaseController;

class PermissionsController extends AppBaseController
{
	/*
	 * API Response JSON
	 *
	 */
	use ApiResponse;
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/permissions",
    * 	  tags={"Permissions"},
    *     operationId="Permissions",
    *     summary="Return list with all permissions",
    *     description="Permissions list",
    *     produces={"application/json"},
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index()
    {
		// Permissions Paginate: page=1
		$data = Permission::latest()->paginate(10);
		
		// Response
    	return $this->respond($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Post(
    *     path="/permissions",
    * 	  tags={"Permissions"},
    *     operationId="addPermission",
    *     summary="Create a new permission for a role",
    *     description="Create a new permission for a role",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="Permission",
    *         in="body",
    *         description="JSON format to create a new permission",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Permissions")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestPermission $request)
    {
		// Input Data
		$input = $request->all();
		
		// News
		$permission = new Permission;
        $permission->name = $input['name'];
        $permission->guard_name = 'api';
		$permission->save();
		
		// Roles
		$roles = (isset($request['roles']) && !empty($request['roles'])) ? $request['roles'] : [];
		foreach ($roles as $id) 
		{
			$role = Role::where('id', '=', $id)->firstOrFail();
			$role->givePermissionTo($permission);
		}
		
		// Response
		return $this->respond(['success' => true, 'message' => 'Created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/permissions/{id}",
    * 	  tags={"Permissions"},
    *     operationId="showPermission",
    *     summary="Show detail of a permission",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of permission",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
		// Find
		if ( ! $permission = Permission::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Response
        return $this->respond(['data' => $permission]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Put(
    *     path="/permissions/{id}",
    * 	  tags={"Permissions"},
    *     operationId="updatePermission",
    *     summary="Update a permission",
    *     description="Update a permission",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="Code of permission",
    * 	  ),
    *     @SWG\Parameter(
    *         name="permission",
    *         in="body",
    *         description="Json format to update a permission",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/PermissionUpdate")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(Request $request, $id)
    {
		// Find
		if ( ! $permission = Permission::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Input Data
		$input = $request->all();
		
		// Update
		$permission->name = $input['name'];
		$permission->save();
		
		// Response
		return $this->respond(['success' => true, 'message' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Delete(
    *     path="/permissions/{id}",
    * 	  tags={"Permissions"},
    *     operationId="DeletePermission",
    *     summary="Delete one permission",
    *     description="Delete one permission",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of permission",
    * 		),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy($id)
    {
		// Find
		if ( ! $permission = Permission::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Destroy
        $permission->delete();
		
		// Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Agent;
use App\Agency;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\API\RequestUser;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\AppBaseController;

class UsersController extends AppBaseController
{
	/*
	 * API Response JSON
	 *
	 */
	use ApiResponse;
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

	/** @SWG\Get(
    *     path="/users",
    * 	  tags={"Users"},
    *     operationId="Users",
    *     summary="Return list with all the users",
    *     description="Users list",
    *     produces={"application/json"},
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index()
    {
		// Users Paginate: page=1
		$data = User::with('roles', 'agent')->latest()->paginate(10);
		
		// Response
    	return $this->respond($data);
    }

	public function getInput($input, $key, $type = '') 
	{
		return (isset($input[$key]) && !empty($input[$key])) ? $input[$key] : $type;
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

	/** @SWG\Post(
    *     path="/users",
    * 	  tags={"Users"},
    *     operationId="addUser",
    *     summary="Create a new user and asign roles or agency if needed",
    *     description="Create a new user and asign roles or agency if needed",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="User",
    *         in="body",
    *         description="JSON format to create a new user",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Users")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestUser $request)
    {
		// Input Data
		$input = $request->all();
		
		// News
		$user = new User;
		$user->first_name 	= $input['first_name'];
		$user->last_name 	= $input['last_name'];
		$user->enabled		= $input['enabled'];
		$user->email 		= $input['email'];
		
		$user->save();
		
		// Roles - Add
		if (isset($input['roles'])) {
			$roles = $this->getInput($input, 'roles', []);
			if (!empty($roles)) {
				foreach ($roles as $id) {
					$role = Role::where('id', '=', $id)->firstOrFail();
					$user->assignRole($role);
				}
			}
		}

		// Agent - Add
		if(isset($input['agency_id']))
		{
			if ( ! $agency = Agency::find($input['agency_id']))
			{
				$id = $input['agency_id'];
				return $this->respondNotFound("The agency with id $id does not exist");
			}

			$agent = New Agent;
			$agent->user_id = $user->id;
			$agent->agency_id = $input['agency_id'];
			$agent->save();

			return $this->respond(['success' => true, 'message' => 'Created successfully', 'agent_id' => $agent->id, 'user_id' => $user->id]);
		}
		
		// Response
		return $this->respond(['success' => true, 'message' => 'Created successfully', 'id' => $user->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */

	/** @SWG\Get(
    *     path="/users/{id}",
    * 	  tags={"Users"},
    *     operationId="showUser",
    *     summary="Show detail of a user",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of user",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
		// Find
		if ( ! $user = User::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		$user = User::with('roles', 'agent')->find($id);

		// Response
        return $this->respond(['data' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */

	/** @SWG\Put(
    *     path="/users/{id}",
    * 	  tags={"Users"},
    *     operationId="updateUser",
    *     summary="Update an user",
    *     description="Update an user",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="Code of user",
    * 	  ),
    *     @SWG\Parameter(
    *         name="user",
    *         in="body",
    *         description="Json format to update an user",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Users")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(RequestUser $request, $id)
    {
		// Find
		if ( ! $user = User::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Input Data
		$input = $request->all();
		
		// Update
		$user->first_name 	= $input['first_name'];
		$user->last_name 	= $input['last_name'];
		$user->email 		= $input['email'];
		$user->enabled		= $input['enabled'];
		
		$user->save();
		
		// Roles - Update
		if (isset($input['roles'])) {
			$roles = $this->getInput($input, 'roles', []);
			if (!empty($roles)) {
				$user->roles()->sync($roles);
			} else {
				$user->roles()->detach();
			}
		}
		
		// Response
		return $this->respond(['success' => true, 'message' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */

	/** @SWG\Delete(
    *     path="/users/{id}",
    * 	  tags={"Users"},
    *     operationId="DeleteUser",
    *     summary="Delete one user",
    *     description="Delete one user",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of user",
    * 		),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy($id)
    {
		// Find
		if ( ! $user = User::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Destroy
        $user->delete();
		
		// Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

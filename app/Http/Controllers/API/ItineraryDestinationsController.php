<?php

namespace App\Http\Controllers\API;

use App\ItineraryDestination;
use Illuminate\Http\Request;
use App\Http\Requests\API\RequestItineraryDestination;
use App\Http\Controllers\AppBaseController;

class ItineraryDestinationsController extends AppBaseController
{
	/*
	 * API Response JSON
	 *
	 */
	use ApiResponse;
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/itineraries/destinations",
    * 	  tags={"ItineraryDestinations"},
    *     operationId="itinerariesDestinations",
    *     summary="Return list with all destinations of itineraries available",
    *     description="Destinations list",
    *     produces={"application/json"},
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index()
    {
        // Itineraries Paginate: page=1
        $data = ItineraryDestination::latest()->paginate(10);
		
        // Response
        return $this->respond($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Post(
    *     path="/itineraries/destinations",
    * 	  tags={"ItineraryDestinations"},
    *     operationId="addDestination",
    *     summary="Create a new destination for an itinerary",
    *     description="Create a new destination for an itinerary",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="ItineraryDestination",
    *         in="body",
    *         description="JSON format to create a new destination",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/ItineraryDestinations")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestItineraryDestination $request)
    {
		// Input Data
        $input = $request->all();

        // News
        $itinerary = new ItineraryDestination();
        $itinerary->itinerary_id 	= $input['itinerary_id'];
		$itinerary->iata_code 		= $input['iata_code'];
        $itinerary->destination_info = json_encode($input['destination_info']);
        $itinerary->save();

        // Response
        return $this->respond(['success' => true, 'message' => 'Created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItineraryDestination  $itinerary
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/itineraries/destinations/{id}",
    * 	  tags={"ItineraryDestinations"},
    *     operationId="showDestination",
    *     summary="Show detail of an itinerary destination",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of the destination",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
        // Find
		if ( ! $itinerary = ItineraryDestination::with(['agent', 'clients'])->find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Response
        return $this->respond(['data' => $itinerary]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItineraryDestination  $itinerary
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItineraryDestination  $itinerary
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Put(
    *     path="/itineraries/destinations/{id}",
    * 	  tags={"ItineraryDestinations"},
    *     operationId="updateDestination",
    *     summary="Update an itinerary destination",
    *     description="Update an itinerary destination",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="Code of destination",
    * 	  ),
    *     @SWG\Parameter(
    *         name="itineraryDestination",
    *         in="body",
    *         description="Json format to update an itinerary destination",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Itineraries")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(RequestItineraryDestination $request, $id)
    {
        // Find
		if ( ! $itinerary = ItineraryDestination::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}

        // Input Data
        $input = $request->all();

        // Update
		$itinerary->iata_code 		= $input['iata_code'];
        $itinerary->destination_info = json_encode($input['destination_info']);
        $itinerary->save();

        // Response
        return $this->respond(['success' => true, 'message' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItineraryDestination  $itinerary
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Delete(
    *     path="/itineraries/destinations/{id}",
    * 	  tags={"ItineraryDestinations"},
    *     operationId="DeleteDestination",
    *     summary="Delete one itinerary destination",
    *     description="Delete one itinerary destination",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of destination",
    * 		),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy($id)
    {
        // Find
		if ( ! $itinerary = ItineraryDestination::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Destroy
        $itinerary->delete();

        // Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

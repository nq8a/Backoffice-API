<?php

namespace App\Http\Controllers\API;

use App\Itinerary;
use App\ItineraryDestination;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests\API\RequestItinerary;
use App\Http\Controllers\AppBaseController;

class ItinerariesController extends AppBaseController
{
	/*
	 * API Response JSON
	 *
	 */
	use ApiResponse;
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/itineraries",
    * 	  tags={"Itineraries"},
    *     operationId="itineraries",
    *     summary="Return list with all itineraries available",
    *     description="Itineraries list",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="agent_id",
    * 			in="query", 
    *           required=false,
    *           description="Id of the agent. Not requied",
    * 			type="integer",
    * 	   		@SWG\Schema(
    *	 	      	required={"agent_id"},
    *	            @SWG\Property(property="agent_id", format="integer", type="integer")
    *        ),		   
    *     ),
    * 	  @SWG\Parameter(
    * 			name="active",
    * 			in="query", 
    * 			type="string",
    *           required=false,
    *           description="State of the itineray, 0 for not active, 1 for active. Not required",
    * 	   		@SWG\Schema(
    *	 	      	required={"active"},
    *	            @SWG\Property(property="string", format="string", type="string")
    *        ),		   
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index(Request $request)
    {
        $queryParams = $request->input();
        $search = [];

        //Filters that can apply
        $filters = [
            'agent_id',
            'active'
        ];

        //building query params
        foreach ( $queryParams as $key => $value ) {
            if(in_array($key, $filters)){
                $value = [$key,'=',$value];
                array_push($search,$value);
            }
        }

        // Itineraries Paginate: page=1
        $datas = Itinerary::with('products', 'state', 'destinations')->where($search)->latest()->paginate(10);
		
		foreach ($datas as $rs)
		{
            $this->parseResponse($rs);
        }
		
        // Response
        return $this->respond($datas);
    }

    /**
     * Parse itineraries response.
     *
     * @param \App\Itinerary $itinerary
     */
    protected function parseResponse($itinerary)
    {
        $items_destination = [];
        $destinations   = $itinerary->destinations;
        foreach ($destinations as $destination)
        {
            $item_destination['iata_code'] = $destination->iata_code;
            $item_destination['destination_info'] = json_decode($destination->destination_info);
            $items_destination[] = $item_destination;
        }
        unset($itinerary['destinations']);

        $items = [];
        $products = $itinerary->products;

        foreach ($products as $product) 
		{
			$item = json_decode($product->item, true);
			$item['id'] = $product['uid'];
			$item['itinerary_id'] = $itinerary->id;
			$items[] = $item;
		}
        unset($itinerary['products']);
        
        $itinerary['destination'] = $items_destination;
        $itinerary['items'] = $items;
    }

	public function getInput($input, $key, $type = null) 
	{
		return (isset($input[$key]) && !empty($input[$key])) ? $input[$key] : $type;
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Post(
    *     path="/itineraries",
    * 	  tags={"Itineraries"},
    *     operationId="addItinerary",
    *     summary="Create a new itinerary",
    *     description="Create a new itinerary",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="Itinerary",
    *         in="body",
    *         description="JSON format to create a new itinerary",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Itineraries")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestItinerary $request)
    {
		// Input Data
        $input = $request->all();

        // Find
		$itinerary = Itinerary::where('uid', '=', $input['uid'])->first();
		
		// News / Update
        if (empty($itinerary))
        {
            $itinerary = new Itinerary();
		}
			
		$this->setItinerary($itinerary, $input);

        // Itinerary ID
		$itinerary_id = $itinerary->id;

        // Destinations Array
        $destinations = $this->getInput($input, 'destination');
        $this->setDestinations($destinations, $itinerary_id);
		
		// Products Array
		$products = $this->getInput($input, 'items');
        
        $items = $this->setItems($products, $itinerary_id);

        if(!empty($items))
        {
            // Response
            return $this->respond(['success' => true, 'message' => 'Created successfully', 'id' => $itinerary_id, 'products' => $items]);
        }

        // Response
        return $this->respond(['success' => true, 'message' => 'Created successfully', 'id' => $itinerary->id]);
    }

    /**
     * Set itinerary fields.
     * 
     * @param  App\Itinerary  $itinerary
     * @param  \Illuminate\Http\Request  $input
     */
    protected function setItinerary($itinerary, $input)
    {
        $itinerary->uid 		    = $input['uid'];
		$itinerary->agent_id 	    = $input['agent_id'];
		$itinerary->client_id 	    = $input['client_id'];
		$itinerary->status_id 	    = $input['status_id'];
		$itinerary->title 		    = $this->getInput($input, 'title');
		$itinerary->location 	    = $this->getInput($input, 'location');
		$itinerary->origin 		    = $this->getInput($input, 'origin');
		$itinerary->arrivalDate     = $input['arrivalDate'];
		$itinerary->departureDate   = $input['departureDate'];
		$itinerary->status 		    = $input['status'];
		$itinerary->active 		    = $input['active'];
		
		// Save
        $itinerary->save();
    }

    /**
     * Set Destinations fields.
     * 
     * @param array $destinations
     * @param integer $itinerary_id
     */
    protected function setDestinations($destinations, $itinerary_id)
    {
        if (is_null($destinations)){
            //destroy destinations
            $destination = ItineraryDestination::where('itinerary_id', $itinerary_id)->delete();
        } 
        if (is_array($destinations) && !empty($destinations)) 
		{   
            //destroy destinations 
            $destination = ItineraryDestination::where('itinerary_id', $itinerary_id)->delete();
            foreach ($destinations as $item_destination) 
			{	
                // Find
				$destination = ItineraryDestination::where(['iata_code' => $item_destination['iata_code'], 'itinerary_id' => $itinerary_id])->first();
				
				// New
                if (empty($destination)) 
                {
                    $destination                = new ItineraryDestination();
                    $destination->itinerary_id  = $itinerary_id;
                    $destination->iata_code     = $item_destination['iata_code'];
                    $destination->destination_info = json_encode($item_destination['destination_info']);

                    // Save
				    $destination->save();
                } else //Update record
                {
                    $destination->itinerary_id  = $itinerary_id;
                    $destination->iata_code     = $item_destination['iata_code'];
                    $destination->destination_info = json_encode($item_destination['destination_info']);
                    $destination->save();
                }
            }
		}
    }

    /**
     * Set Items fields.
     * 
     * @param array $items
     */
    protected function setItems($products, $itinerary_id)
    {
        $items = [];
        if (is_array($products) && !empty($products)) 
		{
			foreach ($products as $item) 
			{
				// Find
				$product = Product::where(['uid' => $item['id'], 'itinerary_id' => $itinerary_id])->first();
				
				// News / Update
                if (!empty($product)) 
                {
					$product->item = json_encode($item);
                } 
                else 
                {
					$product = new Product();
					$product->uid 			= $item['id'];
					$product->itinerary_id 	= $itinerary_id;
					$product->category_id 	= 1; //$item['category_id'];
					$product->item 			= json_encode($item);
                }

                $item_resp['product_uid'] = $item['id'];
                $items[] = $item_resp;
				
				// Save
				$product->save();
            }
        }
        return $items;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Itinerary  $itinerary
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/itineraries/{id}",
    * 	  tags={"Itineraries"},
    *     operationId="showItinerary",
    *     summary="Show detail of an itinerary",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of the itinerary",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
        // Find
        if ( ! $itinerary = Itinerary::find($id))
        {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		$itinerary = Itinerary::with(['destinations', 'agent', 'products', 'state'])->find($id);
        
        // Setting response acording to request
        $this->parseResponse($itinerary);
		
 		// Response
        return $this->respond(['data' => $itinerary]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Itinerary  $itinerary
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Itinerary  $itinerary
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Put(
    *     path="/itineraries/{id}",
    * 	  tags={"Itineraries"},
    *     operationId="updateItinerary",
    *     summary="Update an itinerary",
    *     description="Update an itinerary",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="Code of itinerary",
    * 	  ),
    *     @SWG\Parameter(
    *         name="itinerary",
    *         in="body",
    *         description="Json format to update an itinerary",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Itineraries")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(RequestItinerary $request, $id)
    {
        // Find
        if ( ! $itinerary = Itinerary::find($id))
        {
			return $this->respondNotFound("The register with id $id does not exist");
		}

        // Input Data
        $input = $request->all();

        // Update
        $this->setItinerary($itinerary, $input);

        // Itinerary ID
		$itinerary_id = $itinerary->id;

        // Destinations Array
        $destinations = $this->getInput($input, 'destination');
        $this->setDestinations($destinations, $itinerary_id);

        // Products Array
		$products = $this->getInput($input, 'items');
        $this->setItems($products, $itinerary_id);

        // Response
        return $this->respond(['success' => true, 'message' => 'Updated successfully', 'id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Itinerary  $itinerary
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Delete(
    *     path="/itineraries/{id}",
    * 	  tags={"Itineraries"},
    *     operationId="DeleteItinerary",
    *     summary="Delete one itinerary",
    *     description="Delete one itinerary",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of itinerary",
    * 		),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy($id)
    {
        // Find
		if ( ! $itinerary = Itinerary::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Destroy
        $itinerary->delete();

        // Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

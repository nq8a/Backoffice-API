<?php

namespace App\Http\Controllers\API;

use App\Agency;
use App\User;
use App\Role;
use App\UserRole;
use App\Agent;
use App\Application;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Requests\API\RequestAgency;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;

class AgenciesController extends AppBaseController
{
	/*
	 * API Response JSON
	 *
	 */
	use ApiResponse;
	
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    /** @SWG\Get(
    *     path="/agencies",
    * 	  tags={"Agencies"},
    *     operationId="agencies",
    *     summary="Return list with all agencies available",
    *     description="Agencies list",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="name",
    * 			in="query", 
    *           required=false,
    *           description="Name of the agency. Not requied",
    * 			type="string",
    * 	   		@SWG\Schema(
    *	 	      	required={"name"},
    *	            @SWG\Property(property="name", format="string", type="string")
    *        ),		   
    *     ),
    * 	  @SWG\Parameter(
    * 			name="status",
    * 			in="query", 
    * 			type="string",
    *           required=false,
    *           description="Status of the agency, 0 for false, 1 for true. Not required",
    * 	   		@SWG\Schema(
    *	 	      	required={"status"},
    *	            @SWG\Property(property="string", format="string", type="string")
    *        ),		   
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index(Request $request)
    {
        $queryParams = $request->input();
        $search = [];

        //Filters that can apply
        $filters = [
            'name',
            'status'
        ];

        //building query params
        foreach ( $queryParams as $key => $value ) {
            if(in_array($key, $filters)){
                $value = [$key,'=',$value];
                array_push($search,$value);
            }
        }

		// Agencys Paginate: page=1
		$data = Agency::where($search)->with('users', 'application')->latest()->paginate(10);
		// Response
        return $this->respond($data);
        
    }

    public function getInput($input, $key) 
	{
		return (isset($input[$key]) && !empty($input[$key]) || $input[$key] ) ? $input[$key] : 1;
	}

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Post(
    *     path="/agencies",
    * 	  tags={"Agencies"},
    *     operationId="addAgency",
    *     summary="Create a new agency",
    *     description="Create a new agency",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="Agency",
    *         in="body",
    *         description="JSON format to create a new agency",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Agencies")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestAgency $request)
    {
		// Input Data
		$input = $request->all();
		
		// News
		$agency                 = new Agency;
		$agency->name 		    = $input['name'];
		$agency->name_contact   = $input['name_contact'];
		$agency->name_business  = $input['name_business'];
		$agency->address 	    = $input['address'];
		$agency->phone 		    = $input['phone'];
		$agency->email 		    = $input['email'];
        $agency->status 	    = $input['status'];
        $agency->tax_id 	    = $input['tax_id'];

        //Check if the application who created the agency exists
        if( ! $Application = Application::find($this->getInput($input, 'created_by')))
        {
            $id = $this->getInput($input, 'created_by');
            return $this->respondNotFound("The Application with id $id does not exist");
        }

        //If not, by default is set to 1 (BackOffice)
        $agency->created_by     = $this->getInput($input, 'created_by');
		$agency->save();
		
		// Response
		return $this->respond(['success' => true, 'message' => 'Created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/agencies/{id}",
    * 	  tags={"Agencies"},
    *     operationId="showAgency",
    *     summary="Show detail of an agency",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of the agency",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
		// Find
		if ( ! $agency = Agency::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
        }
        $agency = Agency::find($id);
        $user = Agency::with('users')->find($id)->users;
        $user1=json_decode($user);
        if($user1!=null){
        foreach ($user1 as $key => $value) {
            foreach ($value as $key => $val) {
                if($key=='id'){
                    $user_id[]=$val;
                }
            }}
            foreach ($user_id as $key => $user_id) {
                $user_rol = DB::table('user_roles')
                            ->select('users.id','users.first_name','users.last_name','users.email','users.created_at','users.updated_at','users.deleted_at','users.enabled','roles.name','user_roles.role_id','agency_user.deleted_at')
                            ->join('roles', 'roles.id', '=', 'user_roles.role_id')
                            ->join('users','users.id','=','user_roles.model_id')
                            ->join('agency_user','agency_user.user_id','=','users.id')
                            ->where('user_roles.model_id',$user_id)
                            ->first();
                
                $agent = DB::table('agency_user')
                ->select('agency_user.id','agency_user.deleted_at','user_id')
                ->join('users', 'users.id', '=', 'users.id')
                ->where('agency_user.user_id',$user_id) 
                ->where('agency_user.agency_id',$id)
                ->first();
                if($agent->deleted_at==null){
                $devolveragent[] = $agent;
                $devolver1[] = $user_rol;
                }else{
                    $devolveragent[] = null;
                    $devolver1[] = null;    
                }
            } 
            $data=[];
            $data['agency']=$agency;
            $data['users']=$devolver1;
            $data['agents']=$devolveragent;
            return $this->respond(["data" => $data]);
        }else{    
            $data=[];
            $data['agency']=$agency;
        return $this->respond(["data" => $data]);}

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Put(
    *     path="/agencies/{id}",
    * 	  tags={"Agencies"},
    *     operationId="updateAgency",
    *     summary="Update an agency",
    *     description="Update an agency",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="Code of agency",
    * 	  ),
    *     @SWG\Parameter(
    *         name="agency",
    *         in="body",
    *         description="Json format to update an agency",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Agencies")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(RequestAgency $request, $id)
    {
		// Find
		if ( ! $agency = Agency::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Input Data
		$input = $request->all();
		
		// Update
		$agency->name 		    = $input['name'];
		$agency->name_contact   = $input['name_contact'];
		$agency->name_business  = $input['name_business'];
		$agency->address 	    = $input['address'];
		$agency->phone 		    = $input['phone'];
		$agency->email 		    = $input['email'];
        $agency->status 	    = $input['status'];
        $agency->tax_id 	    = $input['tax_id'];

        //Check if the application who created the agency exists
        if( ! $Application = Application::find($this->getInput($input, 'created_by')))
        {
            $id = $this->getInput($input, 'created_by');
            return $this->respondNotFound("The Application with id $id does not exist");
        }

        //If not, by default is set to 1 (BackOffice)
        $agency->created_by     = $this->getInput($input, 'created_by');
		$agency->save();
		
		// Response
		return $this->respond(['success' => true, 'message' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agency  $agency
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Delete(
    *     path="/agencies/{id}",
    * 	  tags={"Agencies"},
    *     operationId="DeleteAgency",
    *     summary="Delete one agency",
    *     description="Delete one agency",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of agency",
    * 		),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy($id)
    {
		// Find
		if ( ! $agency = Agency::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Destroy
        $agency->delete();
		
		// Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

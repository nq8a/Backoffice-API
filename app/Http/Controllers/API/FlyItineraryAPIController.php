<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFlyItineraryAPIRequest;
use App\Http\Requests\API\UpdateFlyItineraryAPIRequest;
use App\Models\FlyItinerary;
use App\Repositories\FlyItineraryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class FlyItineraryController
 * @package App\Http\Controllers\API
 */

class FlyItineraryAPIController extends AppBaseController
{
    /*
	 * API Response JSON
	 *
	 */
    use ApiResponse;

    /** @var  FlyItineraryRepository */
    private $flyItineraryRepository;

    public function __construct(FlyItineraryRepository $flyItineraryRepo)
    {
        $this->flyItineraryRepository = $flyItineraryRepo;
    }

    /**
     * Display a listing of the FlyItinerary.
     * GET|HEAD /flyItineraries
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->flyItineraryRepository->pushCriteria(new RequestCriteria($request));
        $this->flyItineraryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $flyItineraries = $this->flyItineraryRepository->paginate($limit = 25, $columns = ['*']);

        return $this->respond($flyItineraries->toArray());
    }

    /**
     * Store a newly created FlyItinerary in storage.
     * POST /flyItineraries
     *
     * @param CreateFlyItineraryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFlyItineraryAPIRequest $request)
    {
        $input = $request->all();

        $flyItineraries = $this->flyItineraryRepository->create($input);

        return $this->respond(['success' => true, 'message' => 'Created successfully', 'data' => $flyItineraries->toArray()]);
    }

    /**
     * Display the specified FlyItinerary.
     * GET|HEAD /flyItineraries/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var FlyItinerary $flyItinerary */
        $flyItinerary = $this->flyItineraryRepository->with('itineraries')->findWithoutFail($id);

        if (empty($flyItinerary->toArray())) {
            return $this->sendError('Fly Itinerary not found');
        }

        return $this->respond(['data' => $flyItinerary->toArray()]);
    }

    /**
     * Update the specified FlyItinerary in storage.
     * PUT/PATCH /flyItineraries/{id}
     *
     * @param  int $id
     * @param UpdateFlyItineraryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFlyItineraryAPIRequest $request)
    {
        $input = $request->all();

        /** @var FlyItinerary $flyItinerary */
        $flyItinerary = $this->flyItineraryRepository->findWithoutFail($id);

        if (empty($flyItinerary->toArray())) {
            return $this->sendError('Fly Itinerary not found');
        }

        $flyItinerary = $this->flyItineraryRepository->update($input, $id);

        return $this->respond(['success' => true, 'message' => 'Updated successfully']);
    }

    /**
     * Remove the specified FlyItinerary from storage.
     * DELETE /flyItineraries/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var FlyItinerary $flyItinerary */
        $flyItinerary = $this->flyItineraryRepository->findWithoutFail($id);

        if (empty($flyItinerary->toArray())) {
            return $this->respond(['success' => false, 'message' => 'Fly Itinerary not found']);
            // return $this->sendError('Fly Itinerary not found');
        }

        $flyItinerary->delete();

        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }

    public function itineraryFly($id)
    {
        $flyItinerary = $this->flyItineraryRepository->with('itineraries')->findByField('itinerary', $id, $columns = ['*']);

        if (empty($flyItinerary->toArray())) {
            return $this->respond(['success' => false, 'message' => 'Fly not found']);
        }
        return $this->respond(['data' => $flyItinerary->toArray()]);
        
    }
}

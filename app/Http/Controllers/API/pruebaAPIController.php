<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatepruebaAPIRequest;
use App\Http\Requests\API\UpdatepruebaAPIRequest;
use App\Models\prueba;
use App\Repositories\pruebaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class pruebaController
 * @package App\Http\Controllers\API
 */

class pruebaAPIController extends AppBaseController
{
    /** @var  pruebaRepository */
    private $pruebaRepository;

    public function __construct(pruebaRepository $pruebaRepo)
    {
        $this->pruebaRepository = $pruebaRepo;
    }

    /**
     * Display a listing of the prueba.
     * GET|HEAD /pruebas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pruebaRepository->pushCriteria(new RequestCriteria($request));
        $this->pruebaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $pruebas = $this->pruebaRepository->all();

        return $this->sendResponse($pruebas->toArray(), 'Pruebas retrieved successfully');
    }

    /**
     * Store a newly created prueba in storage.
     * POST /pruebas
     *
     * @param CreatepruebaAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatepruebaAPIRequest $request)
    {
        $input = $request->all();

        $pruebas = $this->pruebaRepository->create($input);

        return $this->sendResponse($pruebas->toArray(), 'Prueba saved successfully');
    }

    /**
     * Display the specified prueba.
     * GET|HEAD /pruebas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var prueba $prueba */
        $prueba = $this->pruebaRepository->findWithoutFail($id);

        if (empty($prueba)) {
            return $this->sendError('Prueba not found');
        }

        return $this->sendResponse($prueba->toArray(), 'Prueba retrieved successfully');
    }

    /**
     * Update the specified prueba in storage.
     * PUT/PATCH /pruebas/{id}
     *
     * @param  int $id
     * @param UpdatepruebaAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatepruebaAPIRequest $request)
    {
        $input = $request->all();

        /** @var prueba $prueba */
        $prueba = $this->pruebaRepository->findWithoutFail($id);

        if (empty($prueba)) {
            return $this->sendError('Prueba not found');
        }

        $prueba = $this->pruebaRepository->update($input, $id);

        return $this->sendResponse($prueba->toArray(), 'prueba updated successfully');
    }

    /**
     * Remove the specified prueba from storage.
     * DELETE /pruebas/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var prueba $prueba */
        $prueba = $this->pruebaRepository->findWithoutFail($id);

        if (empty($prueba)) {
            return $this->sendError('Prueba not found');
        }

        $prueba->delete();

        return $this->sendResponse($id, 'Prueba deleted successfully');
    }
}

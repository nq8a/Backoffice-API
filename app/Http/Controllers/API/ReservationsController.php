<?php

namespace App\Http\Controllers\API;

use App\Reservation;
use App\ReservationProduct;
use App\ReservationDetail;
use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Requests\API\RequestReservation;
use App\Product;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;

class ReservationsController extends AppBaseController
{
	/*
	 * API Response JSON
	 *
	 */
	use ApiResponse;
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/reservations",
    * 	  tags={"Reservations"},
    *     operationId="Reservations",
    *     summary="Return list with all reservations",
    *     description="Reservations list",
    *     produces={"application/json"},
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index(Request $request)
    {
        $queryParams=$request->input();
        $filters=[
            'name',
            'status'
        ];

        $searchByStatus = [];
        $searchByAgency = [];

         //building query params
        foreach ( $queryParams as $key => $value ) {
            if(in_array($key, $filters)){
                $value = [$key,'=',$value];
                if($key=='status'){
                    $searchByStatus[]=$value;
                }else{
                    $searchByAgency[]=$value;
                }
            }
        }

		$datas = Reservation::where($searchByStatus)->with(['details', 'products'])->latest()->paginate(10);
        $data=[];
        $i=0;
        foreach ($datas as $reservation)
		{
            $this->parseResponse($reservation);

        $producto = DB::table('product_reservation')
        ->select('product_reservation.product_id','products.category_id')
        ->join('reservations','reservations.id','=','product_reservation.product_id')
        ->join('products','products.id','=','product_reservation.product_id')
        ->where('product_reservation.reservation_id',$reservation->id)
        ->first();

        $name = DB::table('product_categorys')
        ->select('product_categorys.name','product_categorys.id')
        ->where('product_categorys.id',$producto->category_id)
        ->first();
        
        $agent = DB::table('reservation_agencie_agents')
        ->select('agency_user.id','users.first_name','users.last_name','agencies.name')
        ->where($searchByAgency)
        ->join('agency_user','reservation_agencie_agents.agent_id','=','agency_user.id')
        ->join('users','users.id','=','agency_user.user_id')
        ->join('agencies','agencies.id','=','agency_user.agency_id')
        ->where('reservation_agencie_agents.reservation_id',$reservation->id)
        ->first();


        $data['reservation'][$i]=$reservation;
        $data['product'][$i]=$name; 
        $data['agents'][$i]=$agent;
        $i++;
        }
        $agentsAux=[];
        $productsAux=[];
        $reservationsAux=[];
        if(!empty($searchByAgency)){
            foreach($data['agents'] as $i => $a){
                if(!empty($a)){
                    $agentsAux[]=$data['agents'][$i];
                    $productsAux[]=$data['product'][$i];
                    $reservationsAux[]=$data['reservation'][$i];
                }
            }
            $data['reservation']=$reservationsAux;
            $data['agents']=$agentsAux;
            $data['product']=$productsAux;
        }


		// Response
    	return $this->respond($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

	public function getInput($input, $key, $type = null) 
	{
		return (isset($input[$key]) && !empty($input[$key])) ? $input[$key] : $type;
    }
    
    /**
     * Parse Reservations response.
     *
     * @param \App\Reservation $reservation
     */
    protected function parseResponse($reservation)
    {
        $reservation['idReservation'] = $reservation->uid;
        unset($reservation['uid']);

        $reservation->location 		= json_decode($reservation->location);
        $reservation->paxList 		= json_decode($reservation->paxList);
        $reservation->cxlPolicy 	= json_decode($reservation->cxlPolicy);

        $products = $reservation->products;
        $items = [];

        foreach ($products as $product) 
		{
            $items[] = [
                'uid' => $product->uid
            ];
        }
        
        unset($reservation['products']);
        $reservation['items'] = $items;

        $details = $reservation->details;

        foreach ($details as $detail) 
		{
            unset($detail['id']);
            unset($detail['reservation_id']);
            $detail->pax = json_decode($detail->pax, true);
        }
    }
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     /** @SWG\Post(
    *     path="/reservations",
    * 	  tags={"Reservations"},
    *     operationId="addReservation",
    *     summary="Create a new reservation",
    *     description="Create a new reservation",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="Reservation",
    *         in="body",
    *         description="JSON format to create a new reservation",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Reservations")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestReservation $request)
    {
        // Input Data
        $input = $request->all();
        
        // Find / News
		$reservation = Reservation::where(['uid' => $input['idReservation']])->first();
		if (empty($reservation)) {
			$reservation = new Reservation;
        }

        $this->setReservation($reservation, $input);
        
		// Items Array
		$items = $this->getInput($input, 'items', []);
        if($id = $this->setItems($items, $reservation->id))
        {
            return $this->respondNotFound("The product with id $id does not exist");
        }

		// Details Array
        $details = $this->getInput($input, 'details', []);
        $id = $this->setDetail($reservation->id, $details);
        

        // Response
		return $this->respond(['success' => true, 'message' => 'Created successfully']);
    }

    /**
     * Set reservation fields.
     * 
     * @param  App\Reservation  $reservation
     * @param  \Illuminate\Http\Request  $input
     */
    protected function setReservation($reservation, $input)
    {
        $reservation->uid 			= $input['idReservation'];
        $reservation->categoryId 	= $input['categoryId'];
        $reservation->commission    = $this->getInput($input, 'commission');
        $reservation->nights        = $this->getInput($input, 'nights');
        $reservation->company       = $this->getInput($input, 'company');
        $reservation->payBefore     = $input['payBefore'];
        $reservation->providerId 	= $input['providerId'];
        $reservation->providerName 	= $input['providerName'];
        $reservation->nameAdapter 	= $input['nameAdapter'];
        $reservation->title 		= $input['title'];
        $reservation->description 	= $this->getInput($input, 'description');
        $reservation->location 		= json_encode($this->getInput($input, 'location', []));
        $reservation->arrival 		= $input['arrival'];
        $reservation->departure 	= $input['departure'];
        $reservation->qtyProduct 	= $input['qtyProduct'];
        $reservation->totalPrice 	= $input['totalPrice'];
        $reservation->paxList 		= json_encode($this->getInput($input, 'paxList', []));
        $reservation->cxlPolicy 	= json_encode($this->getInput($input, 'cxlPolicy', []));
        $reservation->status 		= $this->getInput($input, 'status');
        $reservation->save();
    }

    /**
     * Set Items fields.
     * 
     * @param array $items
     */
    protected function setItems($items, $reservation_id)
    {
        if (is_array($items) && !empty($items)) 
		{
			foreach ($items as $item) 
			{
				if (isset($item['uid']) && !empty($item['uid'])) 
				{
					$product = Product::where('uid', $item['uid'])->first();
					
                    if (empty($product))
                    {
                        $id = $item['uid'];
                        return $id;
                    }

					$reservationProduct = ReservationProduct::where([
						'reservation_id' => $reservation_id, 
						'product_id' => $product->id
					])->first();
					
					if (empty($reservationProduct)) {
						$reservationProduct = new ReservationProduct;
						$reservationProduct->reservation_id = $reservation_id;
						$reservationProduct->product_id 	= $product->id;
						$reservationProduct->save();
					}
				}
			}
        }
        return null;
    }

	/**
     * Set Details fields.
     * 
     * @param integer $reservation_id
     * @param array $details 
     */
	protected function setDetail($reservation_id, $details)
	{
        if (is_array($details) && !empty($details)) 
		{
			foreach ($details as $detail) 
			{
                // Find
                $reservationDetail = ReservationDetail::where([
                    'reservation_id' => $reservation_id
                ])->first();
                
                // New
                if (empty($reservationDetail)) {
                    $reservationDetail = new ReservationDetail;
                    $reservationDetail->reservation_id 	= $reservation_id;
                }
                
                // Insert / Update
                $reservationDetail->roomNumber 		= $this->getInput($detail, 'roomNumber');
                $reservationDetail->roomType 		= $this->getInput($detail, 'roomType');
                $reservationDetail->refPrice 		= $this->getInput($detail, 'refPrice');
                $reservationDetail->pax 			= json_encode($this->getInput($detail, 'pax', []));
                $reservationDetail->save();              
			}
        }
        
	}

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/reservations/{id}",
    * 	  tags={"Reservations"},
    *     operationId="showReservation",
    *     summary="Show detail of a reservation",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="UID of reservation",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
		// Find
		if ( ! $reservation = Reservation::where('uid', $id)->first()) {
			return $this->respondNotFound("The register with id $id does not exist");
        }
        
        $reservation = Reservation::with(['details', 'products'])->where('uid', $id)->first();

        $this->parseResponse($reservation);

        $producto = DB::table('product_reservation')
        ->select('product_reservation.product_id','products.category_id')
        ->join('reservations','reservations.id','=','product_reservation.product_id')
        ->join('products','products.id','=','product_reservation.product_id')
        ->where('product_reservation.reservation_id',$reservation->id)
        ->first();

        $name = DB::table('product_categorys')
        ->select('product_categorys.name','product_categorys.id')
        ->where('product_categorys.id',$producto->category_id)
        ->first();
        
        $agent = DB::table('reservation_agencie_agents')
        ->select('agency_user.id','users.first_name','users.last_name','agencies.name')
        ->join('agency_user','reservation_agencie_agents.agent_id','=','agency_user.id')
        ->join('users','users.id','=','agency_user.user_id')
        ->join('agencies','agencies.id','=','agency_user.agency_id')
        ->where('reservation_agencie_agents.reservation_id',$reservation->id)
        ->first();

        $data=[];
        $data['reservation']=$reservation;
        $data['product']=$name; 
        $data['agents']=$agent;
        // Response
        return $this->respond(['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Put(
    *     path="/reservations/{id}",
    * 	  tags={"Reservations"},
    *     operationId="updateReservation",
    *     summary="Update a reservation",
    *     description="Update a reservation",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="UID of reservation",
    * 	  ),
    *     @SWG\Parameter(
    *         name="reservation",
    *         in="body",
    *         description="Json format to update a reservation",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Reservations")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(RequestReservation $request, $id)
    {
        // Find
		if ( ! $reservation = Reservation::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Input Data
        $input = $request->all();
        
        // Update
        $this->setReservation($reservation, $input);

        // Items Array
		$items = $this->getInput($input, 'items', []);
        if($id = $this->setItems($items, $reservation->id))
        {
            return $this->respondNotFound("The product with id $id does not exist");
        }
        
		// Details Array
		$details = $this->getInput($input, 'details', []);
		$this->setDetail($reservation->id, $details);

        // Response
		return $this->respond(['success' => true, 'message' => 'Updated successfully']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Delete(
    *     path="/reservations/{id}",
    * 	  tags={"Reservations"},
    *     operationId="DeleteReservation",
    *     summary="Delete one reservation",
    *     description="Delete one reservation",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of reservation",
    * 		),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy($id)
    {
		// Find
		if ( ! $reservation = Reservation::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
        // Destroy
        $reservation->delete();
		
		// Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

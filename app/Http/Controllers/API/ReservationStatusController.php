<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\ReservationsStatusController;
use App\Http\Controllers\API\ReservationAgencieAgent;
use Illuminate\Support\Facades\DB;

class ReservationStatusController extends Controller
{
   public function ListForStatus($status){


	$res = DB::table('reservation_agencie_agents')
	->select('reservations.id','reservations.categoryId','reservations.providerId','reservations.providerName','reservations.nameAdapter','reservations.title','reservations.description','reservations.arrival','reservations.departure','reservations.qtyProduct','reservations.totalPrice','reservations.paxList','reservations.cxlPolicy','reservations.location','reservations.status','reservations.created_at','reservations.updated_at','reservations.deleted_at','reservations.uid','reservations.commission','reservations.nights','reservations.payBefore','reservations.company','agencies.id','agencies.name','agencies.name_contact','agencies.name_business','agencies.address','agencies.email','agencies.phone','agencies.status','agencies.created_at','agencies.updated_at','agencies.deleted_at','agencies.created_by','agencies.tax_id')
	->join('reservations','reservations.id','=','reservation_agencie_agents.reservation_id')
	->join('agencies','agencies.id','=','reservation_agencie_agents.agencie_id')
	->get();
   /*	if($status==1){
   				$res = DB::table('reservation_agencie_agents')
		->select('reservations.id','reservations.categoryId','reservations.providerId','reservations.providerName','reservations.nameAdapter','reservations.title','reservations.description','reservations.arrival','reservations.departure','reservations.qtyProduct','reservations.totalPrice','reservations.paxList','reservations.cxlPolicy','reservations.location','reservations.status','reservations.created_at','reservations.updated_at','reservations.deleted_at','reservations.uid','reservations.commission','reservations.nights','reservations.payBefore','reservations.company','agencies.id','agencies.name','agencies.name_contact','agencies.name_business','agencies.address','agencies.email','agencies.phone','agencies.status','agencies.created_at','agencies.updated_at','agencies.deleted_at','agencies.created_by','agencies.tax_id')
		->join('reservations','reservations.id','=','reservation_agencie_agents.reservation_id')
		->join('agencies','agencies.id','=','reservation_agencie_agents.agencie_id')
		->where('reservations.status',$status)
		->get();

		$agent = DB::table('agency_user')
		->select('users.id','users.first_name','users.last_name','users.created_at','users.updated_at','users.deleted_at','users.enabled')
		->join('users','users.id','=','agency_user.user_id')
		->where('agency_user.agency_id',$id)
		->get();

		$product = DB::table('reservation_agencie_agents')
		->select('product_categorys.name','product_categorys.id')
		->join('product_reservation','product_reservation.reservation_id','=','reservation_agencie_agents.reservation_id')
		->join('products','products.id','=','product_reservation.product_id')
		->join('product_categorys','product_categorys.id','=','products.category_id')
		->where('reservation_agencie_agents.agencie_id',$id)
		->get();*/

$reservation['reservation']=$res;
/*$reservation['agents']=$agent;
$reservation['products']=$product;*/

		if($res->isEmpty()){
        return (['success' => false, 'message' => 'This agency does not have reservations']);
        }

  /*      if($res){
        	return ['data' => $reservation];}
   	}else{
   		   				$res1 = DB::table('reservation_agencie_agents')
		->select('reservations.id','reservations.categoryId','reservations.providerId','reservations.providerName','reservations.nameAdapter','reservations.title','reservations.description','reservations.arrival','reservations.departure','reservations.qtyProduct','reservations.totalPrice','reservations.paxList','reservations.cxlPolicy','reservations.location','reservations.status','reservations.created_at','reservations.updated_at','reservations.deleted_at','reservations.uid','reservations.commission','reservations.nights','reservations.payBefore','reservations.company','agencies.id','agencies.name','agencies.name_contact','agencies.name_business','agencies.address','agencies.email','agencies.phone','agencies.status','agencies.created_at','agencies.updated_at','agencies.deleted_at','agencies.created_by','agencies.tax_id')
		->join('reservations','reservations.id','=','reservation_agencie_agents.reservation_id')
		->join('agencies','agencies.id','=','reservation_agencie_agents.agencie_id')
		->where('reservations.status',$status)
		->get();

		$agent = DB::table('agency_user')
		->select('users.id','users.first_name','users.last_name','users.created_at','users.updated_at','users.deleted_at','users.enabled')
		->join('users','users.id','=','agency_user.user_id')
		->where('agency_user.agency_id',$id)
		->get();

		$product = DB::table('reservation_agencie_agents')
		->select('product_categorys.name','product_categorys.id')
		->join('product_reservation','product_reservation.reservation_id','=','reservation_agencie_agents.reservation_id')
		->join('products','products.id','=','product_reservation.product_id')
		->join('product_categorys','product_categorys.id','=','products.category_id')
		->where('reservation_agencie_agents.agencie_id',$id)
		->get();*/

$reservation['reservation']=$res;
/*$reservation['agents']=$agent;
$reservation['products']=$product;
   	}*/
		if($res->isEmpty()){
        return (['success' => false, 'message' => 'This agency does not have reservations']);
        }

        if($res){
        	return ['data' => $res];}
   }
}

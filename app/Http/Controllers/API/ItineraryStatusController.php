<?php

namespace App\Http\Controllers\API;

use App\ItineraryStatus;
use Illuminate\Http\Request;
use App\Http\Requests\API\RequestItineraryStatus;
use App\Http\Controllers\AppBaseController;

class ItineraryStatusController extends AppBaseController
{
	/*
	 * API Response JSON
	 *
	 */
	use ApiResponse;
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/itineraries/status",
    * 	  tags={"ItineraryStatus"},
    *     operationId="itineraryStatus",
    *     summary="Return list with all satus available",
    *     description="Status list",
    *     produces={"application/json"},
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index()
    {
        // Itineraries Paginate: page=1
        $data = ItineraryStatus::latest()->paginate(10);
		
        // Response
        return $this->respond($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Post(
    *     path="/itineraries/status",
    * 	  tags={"ItineraryStatus"},
    *     operationId="addStatus",
    *     summary="Create a new stuatus for an itinerary",
    *     description="Create a new status for an itinerary",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="ItineraryStatus",
    *         in="body",
    *         description="JSON format to create a new status",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/ItineraryStatus")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestItineraryStatus $request)
    {
		// Input Data
        $input = $request->all();

        // News
        $itinerary = new ItineraryStatus();
        $itinerary->name 		= $input['name'];
        $itinerary->description = $input['description'];
        $itinerary->save();

        // Response
        return $this->respond(['success' => true, 'message' => 'Created successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItineraryStatus  $itinerary
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/itineraries/status/{id}",
    * 	  tags={"ItineraryStatus"},
    *     operationId="showStatus",
    *     summary="Show detail of an status",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of status",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
        // Find
		if ( ! $itinerary = ItineraryStatus::with(['agent', 'clients'])->find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Response
        return $this->respond(['data' => $itinerary]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItineraryStatus  $itinerary
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItineraryStatus  $itinerary
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Put(
    *     path="/itineraries/status/{id}",
    * 	  tags={"ItineraryStatus"},
    *     operationId="updateStatus",
    *     summary="Update an status",
    *     description="Update an satus",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="Code of status",
    * 	  ),
    *     @SWG\Parameter(
    *         name="ItineraryStatus",
    *         in="body",
    *         description="Json format to update an status",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/ItineraryStatus")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(RequestItineraryStatus $request, $id)
    {
        // Find
		if ( ! $itinerary = ItineraryStatus::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}

        // Input Data
        $input = $request->all();

        // Update
        $itinerary->name 		= $input['name'];
        $itinerary->description = $input['description'];
        $itinerary->save();

        // Response
        return $this->respond(['success' => true, 'message' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItineraryStatus  $itinerary
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Delete(
    *     path="/itineraries/status/{id}",
    * 	  tags={"ItineraryStatus"},
    *     operationId="DeleteStatus",
    *     summary="Delete one status",
    *     description="Delete one satus",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of status",
    * 		),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy($id)
    {
        // Find
		if ( ! $itinerary = ItineraryStatus::find($id)) {
			return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Destroy
        $itinerary->delete();

        // Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

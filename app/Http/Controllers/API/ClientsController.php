<?php

namespace App\Http\Controllers\API;

use App\Agency;
use App\Client;
use Illuminate\Http\Request;
use App\Http\Requests\API\RequestClient;
use App\Http\Controllers\AppBaseController;

class ClientsController extends AppBaseController
{
    /*
	 * API Response JSON
	 *
	 */
    use ApiResponse;
    	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/clients",
    * 	  tags={"Clients"},
    *     operationId="clients",
    *     summary="Return list with all clients available",
    *     description="Clients list",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="name",
    * 			in="query", 
    *           required=false,
    *           description="Name of the Client. Not requied",
    * 			type="string",
    * 	   		@SWG\Schema(
    *	 	      	required={"name"},
    *	            @SWG\Property(property="name", format="string", type="string")
    *        ),		   
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function index(Request $request)
    {
        // Clients Paginate: page=1
        $data = Client::with(['itineraries'])->name($request->get('name'))->latest()->paginate(10);

        // Response
        return $this->respond($data);
    }

    public function getInput($input, $key, $type = null) 
	{
		return (isset($input[$key]) && !empty($input[$key])) ? $input[$key] : $type;
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Add client to an agency
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     /** @SWG\Post(
    *     path="/clients/agency",
    * 	  tags={"Clients"},
    *     operationId="addClientToAgency",
    *     summary="Add a client to an agency",
    *     description="Add a client to an agency",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="Client",
    *         in="body",
    *         description="JSON format to Add a client to an agency",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/AgencyClient")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function addToAgency(Request $request)
    { 
        // Input Data
        $input = $request->all();

        // Find agency
		if ( ! $agency = Agency::find($request->agency_id)) {
            return $this->respondNotFound("The agency with id $request->agency_id does not exist");
        }
        
        // Find client
        if ( ! $client = Client::find($request->client_id)) {
            return $this->respondNotFound("The client with id $request->client_id does not exist");
        }

        $client->agencies()->attach($agency);

        // Response
        return $this->respond(['success' => true, 'message' => 'Client added successfully']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Post(
    *     path="/clients",
    * 	  tags={"Clients"},
    *     operationId="addClient",
    *     summary="Create a new client",
    *     description="Create a new client",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    *     @SWG\Parameter(
    *         name="Client",
    *         in="body",
    *         description="JSON format to create a new client",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Clients")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function store(RequestClient $request)
    {
		// Input Data
        $input = $request->all();

        // News
        $client = new Client;
        $client->first_name = $input['first_name'];
        $client->last_name 	= $this->getInput($input, 'last_name');
        $client->email 		= $input['email'];
        $client->phone 		= $input['phone'];
        $client->birth_date = $this->getInput($input, 'birth_date');
        $client->country 	= $this->getInput($input, 'country');
        $client->city 		= $this->getInput($input, 'city');
        $client->state 		= $this->getInput($input, 'state');
        $client->address 	= $this->getInput($input, 'address');

        $client->save();

        $client_id = $client->id;

        // Response
        return $this->respond(['success' => true, 'message' => 'Created successfully', 'id' => $client_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Get(
    *     path="/clients/{id}",
    * 	  tags={"Clients"},
    *     operationId="showClient",
    *     summary="Show detail of a client",
    *     description="Show details",
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of the client",
    * 	    ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function show($id)
    {
		// Find
		if ( ! $client = Client::find($id)) {
            return $this->respondNotFound("The register with id $id does not exist");
		}
		
        // Response
        return $this->respond(['data' => $client]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Put(
    *     path="/clients/{id}",
    * 	  tags={"Clients"},
    *     operationId="updateClient",
    *     summary="Update a client",
    *     description="Update a client",
    *     consumes={"application/json"},
    *     produces={"application/json"},
    * 	  @SWG\Parameter(
    *   	  name="id",
    * 		  in="path",
    * 		  required=true,
    * 		  type="integer",
    * 		  description="Code of client",
    * 	  ),
    *     @SWG\Parameter(
    *         name="client",
    *         in="body",
    *         description="Json format to update a client",
    *         required=true,
    *         @SWG\Schema(ref="#/definitions/Clients")
    *     ),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function update(RequestClient $request, $id)
    {
        // Find
		if ( ! $client = Client::find($id)) {
            return $this->respondNotFound("The register with id $id does not exist");
		}

        // Input Data
        $input = $request->all();

        // Update
        $client->first_name = $input['first_name'];
        $client->last_name 	= $this->getInput($input, 'last_name');
        $client->email 		= $input['email'];
        $client->phone 		= $input['phone'];
        $client->birth_date = $this->getInput($input, 'birth_date');
        $client->country 	= $this->getInput($input, 'country');
        $client->city 		= $this->getInput($input, 'city');
        $client->state 		= $this->getInput($input, 'state');
        $client->address 	= $this->getInput($input, 'address');

        $client->save();

        // Response
        return $this->respond(['success' => true, 'message' => 'Updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */

    /** @SWG\Delete(
    *     path="/clients/{id}",
    * 	  tags={"Clients"},
    *     operationId="DeleteClient",
    *     summary="Delete one client",
    *     description="Delete one client",
    *     produces={"application/json"},
    * 		@SWG\Parameter(
    * 			name="id",
    * 			in="path",
    * 			required=true,
    * 			type="integer",
    * 			description="Code of client",
    * 		),
    *     @SWG\Response(
    *         response=200,
    *         description="Response of search query",
    *     ),
    *     @SWG\Response(
    *         response=400,
    *         description="Bad request, some field is required",
    *     ),
    *     @SWG\Response(
    *         response=500,
    *         description="Internal error",
    *     ),
    *     @SWG\Response(
    *         response=401,
    *         description="Unauthorized"
    *     )
    * )
    */
    public function destroy($id)
    {
        // Find
		if ( ! $client = Client::find($id)) {
            return $this->respondNotFound("The register with id $id does not exist");
		}
		
		// Destroy
        $client->delete();

        // Response
        return $this->respond(['success' => true, 'message' => 'Deleted successfully']);
    }
}

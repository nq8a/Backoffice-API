<?php
namespace App\Http\Controllers\API;
use App\Agent;
use App\Agency;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\API\RequestAgent;
use App\Http\Controllers\AppBaseController;
use GuzzleHttp\Client;
use App\Http\Controllers\API\ReservationsAgenciesController;
use App\Http\Controllers\API\ReservationAgencieAgent;
use Illuminate\Support\Facades\DB;

class ProviderAgencyController extends AppBaseController
{
	public function test($id)
    {
    	$resp=[];
    	/*Se recuperan los providersIds por agencia*/
    	$res = DB::table('reservation_agencie_agents')
		->select('reservations.providerId','reservations.providerName','agency_user.agency_id','agencies.name')
		->join('reservations','reservations.id','=','reservation_agencie_agents.reservation_id')
		->join('agency_user','agency_user.id','=','reservation_agencie_agents.agent_id')
		->join('agencies','agencies.id','=','agency_user.agency_id')
		->where('agency_user.agency_id',$id)
		->get();
		$res=(array) $res;

		foreach ($res as $i=>$r) {
    		foreach ($r as $value) {
    			$providerIds[] = $value;
    		}
    	}
    	/*Se recuperan los datos de los providersIds*/
    	$client = new \GuzzleHttp\Client([
    		'base_uri'=>'http://octopus-test.usblick.com/api/v1/',
    		'timeout'=>600.0,
    		'headers' => [
                    'Content-Type' => 'application/json',
                    'cache-control' => 'no-cache',
                    'client-id'=> 'I14t5G6uMuXwiiIb',
        			'client-secret'=> "O6079tDaYkc73q3aAmQNHkW5OHfr5o3ELJvaxcna",
        			'authorization'=>' Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFlMzhlNTcwYzBhYjk5Y2Y4NGZlZTZmMWQxN2I1YzcwZDA0MGJmZGVhNTY5ZGNkM2ExZjVhNjFjZDZhNDJiNzVkOTdmOTU3NDJlNjA3MjQ0In0.eyJhdWQiOiIyIiwianRpIjoiMWUzOGU1NzBjMGFiOTljZjg0ZmVlNmYxZDE3YjVjNzBkMDQwYmZkZWE1NjlkY2QzYTFmNWE2MWNkNmE0MmI3NWQ5N2Y5NTc0MmU2MDcyNDQiLCJpYXQiOjE1MzI2Mzg1MTEsIm5iZiI6MTUzMjYzODUxMSwiZXhwIjoxNTMyNzI0OTEwLCJzdWIiOiI2Iiwic2NvcGVzIjpbXX0.LErnOTiDCUxfaPqkU67EQ-D5pdCfXc8znN8tbpbcec0MNm4E6qmGX39gkQnjq2FWdIQUO_0mDA0_zSVcQZTAzzVtXRzpE5BB5zoJCiFRezMouZPs20uNeXj6_wm5mca_SOav2Ejsa1LnhnvBx_vSvGU9dH52gPUxgw_xVl_P4lNz2UKmLbvDMH1lGczda7XrMlDnu12q2EUq7e3MNs-1RgMGl3ThENTT9wSW-y77cSq6Xw0p9yCqBHfYOYmkg933BwYL5QI5QP-4DUHuHig0vzwEljD-BO9EiZRGhs58JQhc0yz9mslYM7sTvMhP3Rwdh5O8Gx9cpQQoPc0siYHAALlNqxxGHifKi2eE8OjVFLl_2R_umi-Rrc3Jh4tUnKWboM-BQdRBR10M6Oye4_3LMEGSpFumQSFDCsBb32x28i7-NGZuoIOVIcxe0WWNMjpqiRW0mPDXdrf0UBZxR_ASSe2vNNRAF2VDnZVW6IniZFYjDn1GnU5cSiXf3zLq3H8JONBQdGZjJiosopVXCrpmAJPCqBjGmi73EWDlKjIL6JkkImiq3mb0k66fi-y6724PD3Mfv5WuGsiecVVXw-ZFmQfE5lyufGfxAacpULvA57CTbO9iaiw-3oiVrvFH5pPClP2phpNNyZyTRvzmVYKkMeKEMZ73-Ua8nw2ndao1xz4'
            ],
    	]);

    	$response = $client ->request ('GET','app/providers');

    	$providers=json_decode($response->getBody()->getContents());
    	$providers=(array)$providers;

    	/*Se filtran los datos de los providersIds de la agencia*/
    	foreach ($providerIds as $pId) {
    		foreach ($providers["data"] as $pData) {
    			if($pData->id==$pId->providerId){
    				$pData->Agency=$pId;
    				$resp[]=$pData;
    			}		
    		}
    	}
    	return $resp;
    }
}
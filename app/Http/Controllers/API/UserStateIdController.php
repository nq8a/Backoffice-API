<?php

namespace App\Http\Controllers\API;

use App\UserAuth;
use App\Http\Controllers\Controller;

class UserStateIdController extends Controller
{
    public function ListAllUser($id)
    {

      try {
        $users = UserAuth::all(['id','name','email','role','enabled','created_at'])->find($id);
        return response()->json(['res' =>1,'users_auth' =>$users,
                                ], 200);
      } catch (Exception $e) {
          $detalle = 'An error has ocurred';
          return response()->json(['res' =>1,'msg' =>$detalle,
                                  ], 200);
      }
    }
}

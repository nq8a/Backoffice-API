<?php

namespace App\Http\Requests\API;

use App\Http\Requests\API\FormRequest;
use App\Providers\ItineraryValidator;

class RequestItinerary extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'agent_id'      => 'required|exists:agency_user,id',
			'status_id' 	=> 'required|exists:itinerary_status,id',
            'active' 		=> 'required',
            'items'         => 'required_without:title',
            'title'         => 'required_without:items'
		];
    }
}

<?php

namespace App\Http\Requests\API;

use App\Http\Requests\API\FormRequest;

class RequestUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PATCH':
            case 'PUT':
                return [
					'first_name' 	=> 'required|max:255',
                    'last_name' 	=> 'required|max:255',
                    'phone' 		=> 'nullable|phone',
                    'enabled'		=> 'nullable|boolean',
                    'email' 		=> 'nullable|string|email|max:255|unique:users,email,' . $this->id,
                ];
                break;
            default:
                return [
					'first_name' 	=> 'required|max:255',
					'last_name' 	=> 'required|max:255',
                    'phone' 		=> 'nullable|phone',
                    'enabled'       => 'required|boolean',
					'email' 		=> 'nullable|string|email|max:255|unique:users',
				//	'password' 		=> 'required|string|min:6|confirmed', //password_confirmation
                ];
                break;
        }
    }
}

<?php

namespace App\Http\Requests\API;

use App\Client;
use App\Http\Requests\API\FormRequest;

class RequestClient extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PATCH':
            case 'PUT':
                return [
					'first_name' 	=> 'required|max:255',
					'last_name' 	=> 'max:255',
					'email' 		=> 'nullable|string|email|max:255|unique:clients,email,' . $this->id,
                    'phone' 		=> 'nullable|phone',
                    'birth_date'    => 'nullable|date',
					'country' 		=> 'max:255',
					'city' 			=> 'max:255',
					'state' 		=> 'max:255',
					'address' 		=> 'max:255',
                ];
                break;
            default:
                return [
					'first_name' 	=> 'required|max:255',
					'last_name' 	=> 'max:255',
					'email' 		=> 'nullable|string|email|max:255|unique:clients',
                    'phone' 		=> 'nullable|phone',
                    'birth_date'    => 'nullable|date',
					'country' 		=> 'max:255',
					'city' 			=> 'max:255',
					'state' 		=> 'max:255',
					'address' 		=> 'max:255',
                ];
                break;
        }
    }
}

<?php

namespace App\Http\Requests\API;

use App\Http\Requests\API\FormRequest;

class RequestAgency extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PATCH':
            case 'PUT':
                return [
					'name' 		    => 'required|max:255',
					'name_contact'  => 'required|max:255',
					'name_business' => 'required|max:255',
					'address' 	    => 'required|max:255',
					'phone' 	    => 'nullable|phone',
					'email' 	    => 'nullable|string|email|max:255|unique:agencies,email,' . $this->id,
                    'status' 	    => 'required',
                    'tax_id'        => 'required|string|unique:agencies,tax_id,' . $this->id
                ];
                break;
            default:
                return [
					'name' 		    => 'required|max:255',
					'name_contact'  => 'required|max:255',
					'name_business' => 'required|max:255',
					'address' 	    => 'required|max:255',
					'phone' 	    => 'nullable|phone',
					'email' 	    => 'nullable|string|email|max:255|unique:agencies',
                    'status' 	    => 'required',
                    'tax_id'        => 'required|string|unique:agencies'
                ];
                break;
        }
    }
}

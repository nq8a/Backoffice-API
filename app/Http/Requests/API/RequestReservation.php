<?php

namespace App\Http\Requests\API;

use App\Http\Requests\API\FormRequest;

class RequestReservation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		switch ($this->method()) {
            case 'PATCH':
            case 'PUT':
                return [
					'title'         => 'required|max:255',
					'description'   => 'max:255',
					'arrival'       => 'required|max:255',
					'departure'     => 'required|max:255',
					'qtyProduct'    => 'required|max:255',
					'totalPrice'    => 'required|max:255',
					'status'        => 'required|max:255',
					'paxList'       => 'max:255',
					'cxlPolicy'     => 'max:255',
					'location'      => 'max:255',
				];
                break;
            default:
                return [
					'idReservation'	=> 'required|max:255',
					'categoryId'    => 'required|max:255',
					'providerId'    => 'required|max:255',
					'providerName'  => 'required|max:255',
					'nameAdapter'   => 'required|max:255',
					'title'         => 'required|max:255',
					'description'   => 'max:255',
					'arrival'       => 'required|max:255',
					'departure'     => 'required|max:255',
					'qtyProduct'    => 'required|max:255',
					'totalPrice'    => 'required|max:255',
					'status'        => 'required|max:255',
					'paxList'       => 'max:255',
					'cxlPolicy'     => 'max:255',
					'location'      => 'max:255',
				];
                break;
        }
    }
}

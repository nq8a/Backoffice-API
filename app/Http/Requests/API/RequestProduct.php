<?php

namespace App\Http\Requests\API;

use App\Http\Requests\API\FormRequest;

class RequestProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		switch ($this->method()) {
            case 'PATCH':
            case 'PUT':
                return [
					'item' => 'required',
				];
                break;
            case 'DELETE':
                return [
					'itinerary_id' 	=> 'required|exists:itineraries,id',
				];
                break;
            default:
                return [
					'itinerary_id' 	=> 'required|exists:itineraries,id',
					'category_id' 	=> 'required|exists:product_categorys,id',
					'item'	 		=> 'required',
				];
                break;
        }
    }
}

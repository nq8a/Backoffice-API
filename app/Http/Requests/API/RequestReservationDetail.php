<?php

namespace App\Http\Requests\API;

use App\Http\Requests\API\FormRequest;

class RequestReservationDetail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PATCH':
            case 'PUT':
                return [
                    'reservation_id'    => 'required|exists:reservations,id',
                    'roomNumber'        => 'required|integer',
                    'roomType'          => 'required|max:255',
                    'refPrice'          => 'required|max:255',
                    'pax'               => 'required'
                ];
                break;
            default:
                return [
                    'reservation_id'    => 'required|exists:reservations,id',
                    'roomNumber'        => 'required|integer',
                    'roomType'          => 'required|max:255',
                    'refPrice'          => 'required|max:255',
                    'pax'               => 'required'
                ];
                break;
        }
    }
}

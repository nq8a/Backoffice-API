<?php

namespace App\Http\Requests\API;

use App\Http\Requests\API\FormRequest;

class RequestApplication extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PATCH':
            case 'PUT':
                return [
					'name' 		    => 'required|max:255',
					'description'   => 'required|max:255'
                ];
                break;
            default:
                return [
					'name' 		    => 'required|max:255',
					'description'   => 'required|max:255'
                ];
                break;
        }
    }
}

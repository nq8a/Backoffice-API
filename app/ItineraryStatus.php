<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(definition="ItineraryStatus", type="object", 
 *  allOf={ 
 *      @SWG\Schema(
 *           required={"name"},
 *           @SWG\Property(property="name", format="string", type="string")
 *       ),
 * 		@SWG\Schema(
 *           required={"description"},
 *           @SWG\Property(property="description", format="string", type="string")
 *       )
 *  }
 * )
 */
class ItineraryStatus extends Model
{
    /**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'itinerary_status';
	
	public function agent()
    {
        return $this->belongsTo('App\Agent');
    }
	
	public function clients()
    {
        return $this->belongsTo('App\Client', 'client_id', 'id');
    }
}

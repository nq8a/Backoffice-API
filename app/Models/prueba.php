<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class prueba
 * @package App\Models
 * @version March 19, 2018, 3:39 pm UTC
 *
 * @property string name
 * @property integer number
 */
class prueba extends Model
{
    use SoftDeletes;

    public $table = 'pruebas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'number' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'number' => 'required'
    ];

    
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class FlyItinerary
 * @package App\Models
 * @version March 23, 2018, 12:51 pm UTC
 *
 * @property string airline
 * @property string flyNumber
 * @property date departure
 * @property date arrival
 * @property string airport
 * @property integer itinerary
 */
class FlyItinerary extends Model
{
    use SoftDeletes;

    public $table = 'flyItineraries';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'airline',
        'flyNumber',
        'departure',
        'arrival',
        'airport',
        'itinerary'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'airline' => 'string',
        'flyNumber' => 'string',
        'departure' => 'datetime',
        'arrival' => 'datetime',
        'airport' => 'string',
        'itinerary' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'airline' => 'required|string',
        'flyNumber' => 'required|string',
        'departure' => 'required|date',
        'arrival' => 'required|date',
        'airport' => 'required|string',
        'itinerary' => 'required|integer|exists:itineraries,id|unique:flyItineraries,itinerary'        
    ];

    /**
     * Validation rules update
     *
     * @var array
     */
    public static $rulesUpdate = [
        'airline' => 'string',
        'flyNumber' => 'string',
        'departure' => 'date',
        'arrival' => 'date',
        'airport' => 'string',
        'itinerary' => 'integer|exists:itineraries,id|unique:flyItineraries,itinerary'    
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     **/
    public function itineraries()
    {
        return $this->hasOne(\App\Itinerary::class,'id','itinerary');
    }

    
}

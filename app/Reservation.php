<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(definition="Reservations", type="object", 
 *  	allOf={ 
 * 			@SWG\Schema(
 *           	required={"title"},
 *           	@SWG\Property(property="title", format="string", type="string")
 *       	),
 * 			@SWG\Schema(
 *           	required={"arrival"},
 *           	@SWG\Property(property="arrival", format="string", type="string")
 *       	),
 * 			@SWG\Schema(
 *           	required={"departure"},
 *           	@SWG\Property(property="departure", format="string", type="string")
 *       	),
 * 			@SWG\Schema(
 *           	required={"totalPrice"},
 *           	@SWG\Property(property="totalPrice", format="string", type="string")
 *       	),
 * 			@SWG\Schema(
 *           	required={"status"},
 *           	@SWG\Property(property="status", format="string", type="string")
 *       	)
 *  	},
 * 		@SWG\Property(
 * 			property="idReservation", 
 * 			format="string", 
 * 			type="string"
 * 		),
 * 		@SWG\Property(
 * 			property="items", 
 * 			format="array", 
 * 			type="array",
 * 			description="Json array with UID of product provided by USBlick-app and details",
 *          @SWG\Items(
 *              @SWG\Property(property="uid", format="string", type="string", description="This is UID of a product")
 *          )
 * 		),
 * 		@SWG\Property(
 * 			property="details", 
 * 			format="array", 
 * 			type="array",
 *          @SWG\Items(
 *              @SWG\Property(property="roomNumber", format="integer", type="integer"),
 * 				@SWG\Property(property="roomType", format="string", type="string"),
 * 				@SWG\Property(property="refPrice", format="string", type="string"),
 * 				@SWG\Property(property="pax", format="array", type="array", description="Pax details information",
 * 					@SWG\Items(type="object")
 * 				)
 *          )
 * 		),
 * 		@SWG\Property(
 * 			property="description", 
 * 			format="string", 
 * 			type="string"
 * 		),
 * 		@SWG\Property(
 * 			property="paxList", 
 * 			format="array", 
 * 			type="array",
 * 			@SWG\Items(
 *              type="object"
 *          )
 * 		),
 * 		@SWG\Property(
 * 			property="cxlPolicy", 
 * 			format="object", 
 * 			type="object"
 * 		),
 * 		@SWG\Property(
 * 			property="location", 
 * 			format="object", 
 * 			type="object"
 * 		)
 * )
 */
class Reservation extends Model
{
    /**
	 * SoftDeletingTrait
	 */
	use SoftDeletes;

	/**
	 * SoftDeletingTrait
	 *
	 * @var Date
	 */
    protected $dates = [
		'deleted_at'
	];
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table = 'reservations';
	
	public function details()
    {
        return $this->hasMany('App\ReservationDetail');
    }
	
	public function products()
    {
        return $this->belongsToMany('App\Product');
	}
	
	public function delete()
    {
        $this->products()->detach();

        // delete the reservation
        return parent::delete();
    }
}

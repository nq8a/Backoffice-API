<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function() 
{
	Route::resource('agencies', 'API\AgenciesController');
	Route::resource('agents', 'API\AgentsController');
	Route::resource('multiagencies','API\MultiAgenciesController');
	Route::get('ListAllUser', 'API\UserStateController@ListAllUser');
	Route::get('ListAllUser/{id}', 'API\UserStateIdController@ListAllUser');
	Route::resource('users', 'API\UsersController');
	Route::resource('roles', 'API\RolesController');
	Route::resource('permissions', 'API\PermissionsController');

	Route::resource('applications', 'API\ApplicationsController');

	Route::post('clients/agency', 'API\ClientsController@addToAgency');
	Route::resource('clients', 'API\ClientsController');
	
	Route::resource('itineraries/destinations', 'API\ItineraryDestinationsController');
	Route::resource('itineraries/status', 'API\ItineraryStatusController');
	Route::resource('itineraries', 'API\ItinerariesController');
	
	Route::resource('products/category', 'API\ProductCategorysController');
	Route::resource('products', 'API\ProductsController');

	Route::get('reservations/details/reservation_id/{detail}', 'API\ReservationDetailsController@showByReservation');
	Route::resource('reservations/details', 'API\ReservationDetailsController');
	Route::resource('reservations', 'API\ReservationsController');
	Route::get('reservations/provider/{id}','API\ReservationsProviderController@ListForProvider');
	Route::get('reservations/agencie/{id}', 'API\ReservationsAgenciesController@ListForAgencie');
	Route::get('reservations/status/{status}', 'API\ReservationStatusController@ListForStatus');
	Route::resource('pruebas', 'API\pruebaAPIController');

	Route::resource('fly_itineraries', 'API\FlyItineraryAPIController');
	Route::get('itinerary/{id}/fly', 'API\FlyItineraryAPIController@itineraryFly');

	Route::get('ProvidersAgency/{id}', 'API\ProviderAgencyController@test');
});





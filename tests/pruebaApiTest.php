<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class pruebaApiTest extends TestCase
{
    use MakepruebaTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateprueba()
    {
        $prueba = $this->fakepruebaData();
        $this->json('POST', '/api/v1/pruebas', $prueba);

        $this->assertApiResponse($prueba);
    }

    /**
     * @test
     */
    public function testReadprueba()
    {
        $prueba = $this->makeprueba();
        $this->json('GET', '/api/v1/pruebas/'.$prueba->id);

        $this->assertApiResponse($prueba->toArray());
    }

    /**
     * @test
     */
    public function testUpdateprueba()
    {
        $prueba = $this->makeprueba();
        $editedprueba = $this->fakepruebaData();

        $this->json('PUT', '/api/v1/pruebas/'.$prueba->id, $editedprueba);

        $this->assertApiResponse($editedprueba);
    }

    /**
     * @test
     */
    public function testDeleteprueba()
    {
        $prueba = $this->makeprueba();
        $this->json('DELETE', '/api/v1/pruebas/'.$prueba->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/pruebas/'.$prueba->id);

        $this->assertResponseStatus(404);
    }
}

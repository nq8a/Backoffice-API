<?php

use App\Models\prueba;
use App\Repositories\pruebaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class pruebaRepositoryTest extends TestCase
{
    use MakepruebaTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var pruebaRepository
     */
    protected $pruebaRepo;

    public function setUp()
    {
        parent::setUp();
        $this->pruebaRepo = App::make(pruebaRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateprueba()
    {
        $prueba = $this->fakepruebaData();
        $createdprueba = $this->pruebaRepo->create($prueba);
        $createdprueba = $createdprueba->toArray();
        $this->assertArrayHasKey('id', $createdprueba);
        $this->assertNotNull($createdprueba['id'], 'Created prueba must have id specified');
        $this->assertNotNull(prueba::find($createdprueba['id']), 'prueba with given id must be in DB');
        $this->assertModelData($prueba, $createdprueba);
    }

    /**
     * @test read
     */
    public function testReadprueba()
    {
        $prueba = $this->makeprueba();
        $dbprueba = $this->pruebaRepo->find($prueba->id);
        $dbprueba = $dbprueba->toArray();
        $this->assertModelData($prueba->toArray(), $dbprueba);
    }

    /**
     * @test update
     */
    public function testUpdateprueba()
    {
        $prueba = $this->makeprueba();
        $fakeprueba = $this->fakepruebaData();
        $updatedprueba = $this->pruebaRepo->update($fakeprueba, $prueba->id);
        $this->assertModelData($fakeprueba, $updatedprueba->toArray());
        $dbprueba = $this->pruebaRepo->find($prueba->id);
        $this->assertModelData($fakeprueba, $dbprueba->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteprueba()
    {
        $prueba = $this->makeprueba();
        $resp = $this->pruebaRepo->delete($prueba->id);
        $this->assertTrue($resp);
        $this->assertNull(prueba::find($prueba->id), 'prueba should not exist in DB');
    }
}

<?php

use Faker\Factory as Faker;
use App\Models\prueba;
use App\Repositories\pruebaRepository;

trait MakepruebaTrait
{
    /**
     * Create fake instance of prueba and save it in database
     *
     * @param array $pruebaFields
     * @return prueba
     */
    public function makeprueba($pruebaFields = [])
    {
        /** @var pruebaRepository $pruebaRepo */
        $pruebaRepo = App::make(pruebaRepository::class);
        $theme = $this->fakepruebaData($pruebaFields);
        return $pruebaRepo->create($theme);
    }

    /**
     * Get fake instance of prueba
     *
     * @param array $pruebaFields
     * @return prueba
     */
    public function fakeprueba($pruebaFields = [])
    {
        return new prueba($this->fakepruebaData($pruebaFields));
    }

    /**
     * Get fake data of prueba
     *
     * @param array $postFields
     * @return array
     */
    public function fakepruebaData($pruebaFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'number' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $pruebaFields);
    }
}

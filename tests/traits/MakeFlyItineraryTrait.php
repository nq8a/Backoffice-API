<?php

use Faker\Factory as Faker;
use App\Models\FlyItinerary;
use App\Repositories\FlyItineraryRepository;

trait MakeFlyItineraryTrait
{
    /**
     * Create fake instance of FlyItinerary and save it in database
     *
     * @param array $flyItineraryFields
     * @return FlyItinerary
     */
    public function makeFlyItinerary($flyItineraryFields = [])
    {
        /** @var FlyItineraryRepository $flyItineraryRepo */
        $flyItineraryRepo = App::make(FlyItineraryRepository::class);
        $theme = $this->fakeFlyItineraryData($flyItineraryFields);
        return $flyItineraryRepo->create($theme);
    }

    /**
     * Get fake instance of FlyItinerary
     *
     * @param array $flyItineraryFields
     * @return FlyItinerary
     */
    public function fakeFlyItinerary($flyItineraryFields = [])
    {
        return new FlyItinerary($this->fakeFlyItineraryData($flyItineraryFields));
    }

    /**
     * Get fake data of FlyItinerary
     *
     * @param array $postFields
     * @return array
     */
    public function fakeFlyItineraryData($flyItineraryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $flyItineraryFields);
    }
}

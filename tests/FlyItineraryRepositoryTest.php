<?php

use App\Models\FlyItinerary;
use App\Repositories\FlyItineraryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FlyItineraryRepositoryTest extends TestCase
{
    use MakeFlyItineraryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var FlyItineraryRepository
     */
    protected $flyItineraryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->flyItineraryRepo = App::make(FlyItineraryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateFlyItinerary()
    {
        $flyItinerary = $this->fakeFlyItineraryData();
        $createdFlyItinerary = $this->flyItineraryRepo->create($flyItinerary);
        $createdFlyItinerary = $createdFlyItinerary->toArray();
        $this->assertArrayHasKey('id', $createdFlyItinerary);
        $this->assertNotNull($createdFlyItinerary['id'], 'Created FlyItinerary must have id specified');
        $this->assertNotNull(FlyItinerary::find($createdFlyItinerary['id']), 'FlyItinerary with given id must be in DB');
        $this->assertModelData($flyItinerary, $createdFlyItinerary);
    }

    /**
     * @test read
     */
    public function testReadFlyItinerary()
    {
        $flyItinerary = $this->makeFlyItinerary();
        $dbFlyItinerary = $this->flyItineraryRepo->find($flyItinerary->id);
        $dbFlyItinerary = $dbFlyItinerary->toArray();
        $this->assertModelData($flyItinerary->toArray(), $dbFlyItinerary);
    }

    /**
     * @test update
     */
    public function testUpdateFlyItinerary()
    {
        $flyItinerary = $this->makeFlyItinerary();
        $fakeFlyItinerary = $this->fakeFlyItineraryData();
        $updatedFlyItinerary = $this->flyItineraryRepo->update($fakeFlyItinerary, $flyItinerary->id);
        $this->assertModelData($fakeFlyItinerary, $updatedFlyItinerary->toArray());
        $dbFlyItinerary = $this->flyItineraryRepo->find($flyItinerary->id);
        $this->assertModelData($fakeFlyItinerary, $dbFlyItinerary->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteFlyItinerary()
    {
        $flyItinerary = $this->makeFlyItinerary();
        $resp = $this->flyItineraryRepo->delete($flyItinerary->id);
        $this->assertTrue($resp);
        $this->assertNull(FlyItinerary::find($flyItinerary->id), 'FlyItinerary should not exist in DB');
    }
}

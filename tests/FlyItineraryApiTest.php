<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FlyItineraryApiTest extends TestCase
{
    use MakeFlyItineraryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateFlyItinerary()
    {
        $flyItinerary = $this->fakeFlyItineraryData();
        $this->json('POST', '/api/v1/flyItineraries', $flyItinerary);

        $this->assertApiResponse($flyItinerary);
    }

    /**
     * @test
     */
    public function testReadFlyItinerary()
    {
        $flyItinerary = $this->makeFlyItinerary();
        $this->json('GET', '/api/v1/flyItineraries/'.$flyItinerary->id);

        $this->assertApiResponse($flyItinerary->toArray());
    }

    /**
     * @test
     */
    public function testUpdateFlyItinerary()
    {
        $flyItinerary = $this->makeFlyItinerary();
        $editedFlyItinerary = $this->fakeFlyItineraryData();

        $this->json('PUT', '/api/v1/flyItineraries/'.$flyItinerary->id, $editedFlyItinerary);

        $this->assertApiResponse($editedFlyItinerary);
    }

    /**
     * @test
     */
    public function testDeleteFlyItinerary()
    {
        $flyItinerary = $this->makeFlyItinerary();
        $this->json('DELETE', '/api/v1/flyItineraries/'.$flyItinerary->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/flyItineraries/'.$flyItinerary->id);

        $this->assertResponseStatus(404);
    }
}
